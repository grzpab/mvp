import 'mocha';
import { expect } from 'chai';
import { GameDelta, GameStatus } from '../../../src/types/game';

import { getOutgoingCommands } from '../../../src/outgoingCommands';
import { PlayerStatus } from '../../../src/types/players';
import { CommandType } from '../../../src/commands/commandTypes';

describe('/outgoingCommands/index.ts', () => {
    describe('getOutgoingCommands', () => {
        it('returns no commands if no changes or deltas spotted', () => {
            const gameDelta: GameDelta = {
                newStatus: GameStatus.INITIATED,
                statusChanged: false,
                cellDeltas: [],
                playerDeltas: [],
            };

            const commands = getOutgoingCommands(gameDelta);

            expect(commands.length).to.equal(0);
        });

        it('returns a command if status has changed', () => {
            const gameDelta: GameDelta = {
                newStatus: GameStatus.INITIATED,
                statusChanged: true,
                cellDeltas: [],
                playerDeltas: [],
            };

            const commands = getOutgoingCommands(gameDelta);

            expect(commands.length).to.equal(1);
        });

        it('returns commands if deltas are present', () => {
            const gameDelta: GameDelta = {
                newStatus: GameStatus.INITIATED,
                statusChanged: false,
                cellDeltas: [
                    {
                        id: 0,
                        newOwnerId: 0,
                        ownerIdChanged: true
                    }
                ],
                playerDeltas: [
                    {
                        id: 0,
                        newStatus: PlayerStatus.LOST,
                        newCoordinates: [0, 0],
                        statusChanged: true,
                        coordinatesChanged: false,
                    }
                ],
            };

            const commands = getOutgoingCommands(gameDelta);

            expect(commands.length).to.equal(2);

            expect(commands[0]).to.be.deep.equal({
                commandType: CommandType.CELL_OWNER_CHANGED,
                id: 0,
                newOwnerId: 0,
            });

            expect(commands[1]).to.be.deep.equal({
                commandType: 'PLAYER_LOST',
                id: 0,
            });
        });
    });
});
