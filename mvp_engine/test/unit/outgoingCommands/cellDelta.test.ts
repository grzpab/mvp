import 'mocha';
import { expect } from 'chai';

import { CellDelta } from '../../../src/types/cells';
import { getOutgoingCommandOnCellDelta } from '../../../src/outgoingCommands/cellDelta';
import { CommandType } from '../../../src/commands/commandTypes';

describe('/outgoingCommands/cellDelta.ts', () => {
    describe('getOutgoingCommandOnCellDelta', () => {
        it('returns undefined if ownerId not changed', () => {
            const cellDelta: CellDelta = {
                id: 0,
                newOwnerId: 1,
                ownerIdChanged: false,
            };

            const command = getOutgoingCommandOnCellDelta(cellDelta);

            expect(command).to.be.undefined;
        });

        it('returns a command if ownerId changed', () => {
            const cellDelta: CellDelta = {
                id: 0,
                newOwnerId: 2,
                ownerIdChanged: true,
            };

            const command = getOutgoingCommandOnCellDelta(cellDelta);

            expect(command).to.deep.equal({
                id: 0,
                commandType: CommandType.CELL_OWNER_CHANGED,
                newOwnerId: 2,
            });
        });
    });
});