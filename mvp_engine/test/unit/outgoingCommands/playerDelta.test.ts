import 'mocha';
import { expect } from 'chai';
import {
    PlayerMovedCommand,
    PlayerStatusCommand,
} from '../../../src/commands/outgoingCommands';

import {
    getOutgoingCommandOnPlayerCoordinates,
    getOutgoingCommandOnPlayerStatus,
    getOutgoingCommandsOnPlayerDelta,
} from '../../../src/outgoingCommands/playerDelta';
import { CommandType } from '../../../src/commands/commandTypes';
import { PlayerStatus, PlayerDelta } from '../../../src/types/players';

describe('/outgoingCommands/playerDelta.ts', () => {
    describe('getOutgoingCommandOnPlayerCoordinates', () => {
        it('creates a correct command', () => {
            const command = getOutgoingCommandOnPlayerCoordinates(2, [3, 4]) as PlayerMovedCommand;

            expect(command).not.to.be.undefined;
            expect(command).to.be.deep.equal({
                commandType: CommandType.PLAYER_MOVED,
                id: 2,
                newCoordinates: [3, 4],
            });
        });
    });

    describe('getOutgoingCommandOnPlayerStatus', () => {
        it('creates a correct command if player has been acknowledged', () => {
            const command = getOutgoingCommandOnPlayerStatus(
                1,
                PlayerStatus.ACKNOWLEDGED
            ) as PlayerStatusCommand;

            expect(command).not.to.be.undefined;
            expect(command).to.be.deep.equal({
                id: 1,
                commandType: CommandType.PLAYER_ACKNOWLEDGED,
            });
        });

        it('creates a correct command if player has been created', () => {
            const command = getOutgoingCommandOnPlayerStatus(
                1,
                PlayerStatus.CREATED
            ) as PlayerStatusCommand;

            expect(command).not.to.be.undefined;
            expect(command).to.be.deep.equal({
                id: 1,
                commandType: CommandType.PLAYER_CREATED,
            });
        });

        it('creates a correct command if player has lost', () => {
            const command = getOutgoingCommandOnPlayerStatus(
                1,
                PlayerStatus.LOST
            ) as PlayerStatusCommand;

            expect(command).not.to.be.undefined;
            expect(command).to.be.deep.equal({
                id: 1,
                commandType: CommandType.PLAYER_LOST,
            });
        });

        it('creates a correct command if player is playing', () => {
            const command = getOutgoingCommandOnPlayerStatus(
                1,
                PlayerStatus.PLAYING
            ) as PlayerStatusCommand;

            expect(command).not.to.be.undefined;
            expect(command).to.be.deep.equal({
                id: 1,
                commandType: CommandType.PLAYER_PLAYING,
            });
        });

        it('creates a correct command if player has won', () => {
            const command = getOutgoingCommandOnPlayerStatus(
                1,
                PlayerStatus.WON
            ) as PlayerStatusCommand;

            expect(command).not.to.be.undefined;
            expect(command).to.be.deep.equal({
                id: 1,
                commandType: CommandType.PLAYER_WON,
            });
        });

        it('creates a correct command if player has yielded', () => {
            const command = getOutgoingCommandOnPlayerStatus(
                1,
                PlayerStatus.YIELDED
            ) as PlayerStatusCommand;

            expect(command).not.to.be.undefined;
            expect(command).to.be.deep.equal({
                id: 1,
                commandType: CommandType.PLAYER_YIELDED,
            });
        });

        it('returns undefined if the status is not correct', () => {
            const command = getOutgoingCommandOnPlayerStatus(
                1,
                {} as PlayerStatus,
            ) as PlayerStatusCommand;

            expect(command).to.be.undefined;
            
        });
    });

    describe('getOutgoingCommandsOnPlayerDelta', () => {
        it('returns no commands if no changes spotted', () => {
            const playerDelta: PlayerDelta = {
                id: 1,
                newStatus: PlayerStatus.CREATED,
                newCoordinates: [0, 0],
                statusChanged: false,
                coordinatesChanged: false,
            };

            const commands = getOutgoingCommandsOnPlayerDelta(playerDelta);

            expect(commands.length).to.equal(0);
        });

        it('returns a command if status has changed', () => {
            const playerDelta: PlayerDelta = {
                id: 1,
                newStatus: PlayerStatus.CREATED,
                newCoordinates: [0, 0],
                statusChanged: true,
                coordinatesChanged: false,
            };

            const commands = getOutgoingCommandsOnPlayerDelta(playerDelta);

            expect(commands.length).to.equal(1);

            const command = commands[0];

            expect(command).to.deep.equal({
                id: 1,
                commandType: CommandType.PLAYER_CREATED,
            });
        });

        it('returns a command if coordinates have changed', () => {
            const playerDelta: PlayerDelta = {
                id: 1,
                newStatus: PlayerStatus.CREATED,
                newCoordinates: [0, 0],
                statusChanged: false,
                coordinatesChanged: true,
            };

            const commands = getOutgoingCommandsOnPlayerDelta(playerDelta);

            expect(commands.length).to.equal(1);

            const command = commands[0];

            expect(command).to.deep.equal({
                id: 1,
                commandType: CommandType.PLAYER_MOVED,
                newCoordinates: [0, 0],
            });
        });

        it('returns two commands if both status and coordinates have changed', () => {
            const playerDelta: PlayerDelta = {
                id: 1,
                newStatus: PlayerStatus.CREATED,
                newCoordinates: [0, 0],
                statusChanged: true,
                coordinatesChanged: true,
            };

            const commands = getOutgoingCommandsOnPlayerDelta(playerDelta);

            expect(commands.length).to.equal(2);

            {
                const command = commands[0];

                expect(command).to.deep.equal({
                    id: 1,
                    commandType: CommandType.PLAYER_CREATED,
                });
            }

            {
                const command = commands[1];

                expect(command).to.deep.equal({
                    id: 1,
                    commandType: CommandType.PLAYER_MOVED,
                    newCoordinates: [0, 0],
                });
            }
        });
    });
});