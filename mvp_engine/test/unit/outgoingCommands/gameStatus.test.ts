import 'mocha';
import { expect } from 'chai';

import { getOutgoingCommandOnGameStatus } from '../../../src/outgoingCommands/gameStatus';
import { OutgoingGameCommand } from '../../../src/commands/outgoingCommands';
import { GameStatus } from '../../../src/types/game';
import { CommandType } from '../../../src/commands/commandTypes';

describe('/outgoingCommands/gameStatus.ts', () => {
    describe('getOutgoingCommandOnGameStatus', () => {
        it('returns correct command type for a initiated game status', () => {
            const command = getOutgoingCommandOnGameStatus(GameStatus.INITIATED) as OutgoingGameCommand;
            
            expect(command).not.to.be.undefined;
            expect(command.commandType).to.equal(CommandType.GAME_INITIATED);
        });

        it('returns correct command type for a started game status', () => {
            const command = getOutgoingCommandOnGameStatus(GameStatus.STARTED) as OutgoingGameCommand;
            
            expect(command).not.to.be.undefined;
            expect(command.commandType).to.equal(CommandType.GAME_STARTED);
        });

        it('returns correct command type for a finished game status', () => {
            const command = getOutgoingCommandOnGameStatus(GameStatus.FINISHED) as OutgoingGameCommand;
            
            expect(command).not.to.be.undefined;
            expect(command.commandType).to.equal(CommandType.GAME_FINISHED);
        });

        it('returns undefined command type for a unknown game status', () => {
            const command = getOutgoingCommandOnGameStatus({} as GameStatus) as OutgoingGameCommand;
            
            expect(command).to.be.undefined;
        });
    });
});