import 'mocha';
import { expect } from 'chai';

import {
    GameStatus,
} from '../../../src/types/game';
import { get2x2Game, constructDiagonalGame } from '../../helpers/gameHelpers';
import { Cell } from '../../../src/types/cells';
import { Player } from '../../../src/types/players';

describe('/types/Game.ts', () => {
    it('Game::initiate', () => {
        const game = get2x2Game();

        expect(game.status).to.equal(GameStatus.INITIATED);
        expect(game.size).to.deep.equal([2, 2]);
    });

    it('Game::start', () => {
        const game = get2x2Game().start();

        expect(game.status).to.equal(GameStatus.STARTED);
    });

    it('Game::mergeEntities', () => {
        const oldGame = constructDiagonalGame(3);

        expect(oldGame.getPlayers().length).to.equal(3);
        expect(oldGame.getCells().length).to.equal(9);

        const oldCell = oldGame.getCellByCoordinates([0, 0]) as Cell;
        const newCell = oldCell.setOwnerId(undefined);

        const oldPlayer = oldGame.getPlayer(0) as Player;
        const newPlayer = oldPlayer.makeLose();

        const newGame = oldGame.mergeEntities(
            [ newCell ],
            [ newPlayer ]
        );

        expect(newGame.getPlayers().length).to.equal(3);
        expect(newGame.getCells().length).to.equal(9);

        const cellInQuestion = newGame.getCellByCoordinates([0, 0]);
        const playerInQuestion = newGame.getPlayer(0);

        expect(cellInQuestion).to.equal(newCell);
        expect(cellInQuestion).not.to.equal(oldCell);

        expect(playerInQuestion).to.equal(newPlayer);
        expect(playerInQuestion).not.to.equal(oldPlayer);
    });

    it('Game::tryFinishing', () => {
        // TODO write tests here
    });

    it('Game::getPlayer', () => {
        const oldGame = constructDiagonalGame(2);

        expect(oldGame.getPlayer(-1)).to.be.undefined;
        expect(oldGame.getPlayer(0)).not.to.be.undefined;
        expect(oldGame.getPlayer(1)).not.to.be.undefined;
        expect(oldGame.getPlayer(2)).to.be.undefined;
    });

    it('Game::getCell', () => {
        const oldGame = constructDiagonalGame(2);

        expect(oldGame.getCell(-1)).to.be.undefined;
        expect(oldGame.getCell(0)).not.to.be.undefined;
        expect(oldGame.getCell(3)).not.to.be.undefined;
        expect(oldGame.getCell(4)).to.be.undefined;
    });

    it('Game::getCellByCoordinates', () => {
        const oldGame = constructDiagonalGame(2);

        const existingCell = oldGame.getCellByCoordinates([0, 0]);

        expect(existingCell).not.to.be.undefined;

        expect(oldGame.getCellByCoordinates([4, 4])).to.be.undefined;
    });

    it('Game::filterPlayers', () => {
        const oldGame = constructDiagonalGame(2);
        
        expect(oldGame.filterPlayers(() => true).length).to.equal(2);
        expect(oldGame.filterPlayers(() => false).length).to.equal(0);
        expect(oldGame.filterPlayers(p => p.id > 0).length).to.equal(1);
    });

    it('Game::getPlayingPlayers', () => {
        const oldGame = constructDiagonalGame(2);

        expect(oldGame.getPlayingPlayers().length).to.equal(0);

        const firstPlayer = oldGame.getPlayer(0) as Player;

        const newPlayers = [
            firstPlayer.makePlaying(),
        ];

        const newGame = oldGame.mergeEntities([], newPlayers);

        expect(newGame.getPlayingPlayers().length).to.equal(1);
    });

    it('Game::filterCells', () => {
        const oldGame = constructDiagonalGame(2);

        expect(oldGame.filterCells(() => true).length).to.equal(4);
        expect(oldGame.filterCells(() => false).length).to.equal(0);
        expect(oldGame.filterCells((cell) => cell.ownerId === undefined).length).to.equal(2);
    });

    it('Game::getCells', () => {
        const oldGame = constructDiagonalGame(2);

        expect(oldGame.getCells().length).to.equal(4);
    });

    it('Game::getPlayers', () => {
        const oldGame = constructDiagonalGame(2);

        expect(oldGame.getPlayers().length).to.equal(2);
    });

});