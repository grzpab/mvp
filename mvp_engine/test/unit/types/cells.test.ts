import 'mocha';
import { expect } from 'chai';

import { Cell } from '../../../src/types/cells';

describe('/test/cells.ts', () => {
    describe('Cell::setOwnerId', () => {
        it('sets the ownerId and returns a new object', () => {
            const oldCell = new Cell(1, [2, 2], true, undefined);

            expect(oldCell.ownerId).to.be.undefined;

            const newCell = oldCell.setOwnerId(3);

            expect(oldCell).to.not.equal(newCell);
            expect(newCell.ownerId).to.equal(3);
        });

        it('unsets the ownerId and returns a new object', () => {
            const oldCell = new Cell(1, [2, 2], true, 4);

            expect(oldCell.ownerId).to.equal(4);

            const newCell = oldCell.setOwnerId(undefined);

            expect(oldCell).to.not.equal(newCell);
            expect(newCell.ownerId).to.be.undefined;
        });
    });
});