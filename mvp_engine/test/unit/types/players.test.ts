import 'mocha';
import { expect } from 'chai';

import { Player, PlayerStatus } from '../../../src/types/players';

describe('/types/player.ts', () => {
    const expectIdMatch = (oldPlayer: Player, newPlayer: Player) => {
        expect(oldPlayer.id).to.equal(2);
        expect(newPlayer.id).to.equal(2);
    };

    const expectCoordinatesMatch = (oldPlayer: Player, newPlayer: Player) => {
        expect(oldPlayer.coordinates).to.deep.equal([3, 4]);
        expect(newPlayer.coordinates).to.deep.equal([3, 4]);
    };

    describe('Player::create', () => {
        it('creates a new player', () => {
            const player = Player.create(2, [3, 4]);

            expect(player).not.to.be.undefined;
            expect(player.id).to.equal(2);
            expect(player.coordinates).to.deep.equal([3, 4]);
            expect(player.status).to.equal(PlayerStatus.CREATED);
        });
    });

    describe('Player::setCoordinates', () => {
        it('creates a new player with new coordinates', () => {
            const oldPlayer = Player.create(2, [3, 4]);
            const newPlayer = oldPlayer.setCoordinates([1, 3]);

            expect(newPlayer).not.to.equal(oldPlayer);

            expectIdMatch(oldPlayer, newPlayer);

            expect(oldPlayer.coordinates).to.deep.equal([3, 4]);
            expect(newPlayer.coordinates).to.deep.equal([1, 3]);

            expect(oldPlayer.status).to.equal(PlayerStatus.CREATED);
            expect(newPlayer.status).to.equal(PlayerStatus.CREATED);
        });
    });

    describe('Player::acknowledge', () => {
        it('creates a new acknowledged player', () => {
            const oldPlayer = Player.create(2, [3, 4]);
            const newPlayer = oldPlayer.acknowledge();

            expect(newPlayer).not.to.equal(oldPlayer);

            expectIdMatch(oldPlayer, newPlayer);
            expectCoordinatesMatch(oldPlayer, newPlayer);

            expect(oldPlayer.status).to.equal(PlayerStatus.CREATED);
            expect(newPlayer.status).to.equal(PlayerStatus.ACKNOWLEDGED);
        });
    });

    describe('Player::makePlaying', () => {
        it('creates a new acknowledged player', () => {
            const oldPlayer = Player.create(2, [3, 4]);
            const newPlayer = oldPlayer.makePlaying();

            expect(newPlayer).not.to.equal(oldPlayer);

            expectIdMatch(oldPlayer, newPlayer);
            expectCoordinatesMatch(oldPlayer, newPlayer);

            expect(oldPlayer.status).to.equal(PlayerStatus.CREATED);
            expect(newPlayer.status).to.equal(PlayerStatus.PLAYING);
        });
    });

    describe('Player::makeYield', () => {
        it('creates a new acknowledged player', () => {
            const oldPlayer = Player.create(2, [3, 4]);
            const newPlayer = oldPlayer.makeYield();

            expect(newPlayer).not.to.equal(oldPlayer);

            expectIdMatch(oldPlayer, newPlayer);

            expect(oldPlayer.coordinates).to.deep.equal([3, 4]);
            expect(newPlayer.coordinates).to.be.undefined;

            expect(oldPlayer.status).to.equal(PlayerStatus.CREATED);
            expect(newPlayer.status).to.equal(PlayerStatus.YIELDED);
        });
    });

    describe('Player::makeLose', () => {
        it('creates a new loser', () => {
            const oldPlayer = Player.create(2, [3, 4]);
            const newPlayer = oldPlayer.makeLose();

            expect(newPlayer).not.to.equal(oldPlayer);

            expectIdMatch(oldPlayer, newPlayer);
            expectCoordinatesMatch(oldPlayer, newPlayer);

            expect(oldPlayer.status).to.equal(PlayerStatus.CREATED);
            expect(newPlayer.status).to.equal(PlayerStatus.LOST);
        });
    });

    describe('Player::makeWin', () => {
        it('creates a new winner', () => {
            const oldPlayer = Player.create(2, [3, 4]);
            const newPlayer = oldPlayer.makeWin();

            expect(newPlayer).not.to.equal(oldPlayer);

            expectIdMatch(oldPlayer, newPlayer);
            expectCoordinatesMatch(oldPlayer, newPlayer);

            expect(oldPlayer.status).to.equal(PlayerStatus.CREATED);
            expect(newPlayer.status).to.equal(PlayerStatus.WON);
        });
    });
});
