import 'mocha';
import { expect } from 'chai';

import {
    calculateNewCoordinates,
    Direction,
    Coordinates
} from '../../../src/types/coordinates';

describe('/types/coordinates.ts', () => {
    describe('calculateNewCoordinates', () => {
        it('calculates the northern cell', () => {
            const oldCoordinates: Coordinates = [3, 3];

            const newCoordinates = calculateNewCoordinates(oldCoordinates, Direction.NORTH);

            expect(oldCoordinates).not.to.equal(newCoordinates);
            expect(newCoordinates).to.be.deep.equal([3, 2]);
        });

        it('calculates the southern cell', () => {
            const oldCoordinates: Coordinates = [4, 5];

            const newCoordinates = calculateNewCoordinates(oldCoordinates, Direction.SOUTH);

            expect(oldCoordinates).not.to.equal(newCoordinates);
            expect(newCoordinates).to.be.deep.equal([4, 6]);
        });

        it('calculates the western cell', () => {
            const oldCoordinates: Coordinates = [2, 1];

            const newCoordinates = calculateNewCoordinates(oldCoordinates, Direction.WEST);

            expect(oldCoordinates).not.to.equal(newCoordinates);
            expect(newCoordinates).to.be.deep.equal([1, 1]);
        });

        it('calculates the eastern cell', () => {
            const oldCoordinates: Coordinates = [7, 4];

            const newCoordinates = calculateNewCoordinates(oldCoordinates, Direction.EAST);

            expect(oldCoordinates).not.to.equal(newCoordinates);
            expect(newCoordinates).to.be.deep.equal([8, 4]);
        });
    });
});
