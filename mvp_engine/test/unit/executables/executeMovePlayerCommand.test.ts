import 'mocha';
import { expect } from 'chai';

import {
    executeMovePlayerCommand,
    destroyOldPlayer,
    destroyBothPlayers,
    destroyOtherPlayerAndMoveOldPlayer,
    movePlayerIntoOwnerlessCell
} from '../../../src/executables/executeMovePlayerCommand';
import { CommandType } from '../../../src/commands/commandTypes';
import { Direction, Coordinates } from '../../../src/types/coordinates';
import { constructDiagonalGame } from '../../helpers/gameHelpers';
import { MovePlayerCommand } from '../../../src/commands/incomingCommands';
import {
    GameDoesNotExistError,
    IncorrectGameStatusError,
    PlayerNotFoundError,
    IncorrectPlayerStatusError,
    PlayerHasNoCoordinatesError,
    CellNotFoundError,
    CellNotAccessibleError
} from '../../../src/errors';
import { Player, PlayerStatus } from '../../../src/types/players';
import { Cell } from '../../../src/types/cells';
import { GameStatus } from '../../../src/types/game';

describe('/executables/executeMovePlayerCommand.ts', () => {
    const command: MovePlayerCommand = {
        commandType: CommandType.MOVE_PLAYER,
        direction: Direction.SOUTH,
        playerId: 0,
    };

    describe('executeMovePlayerCommand', () => {
        it('throws GameDoesNotExistError if game is undefined', () => {
            const oldGame = undefined;
            
            expect(
                () => executeMovePlayerCommand(command, oldGame)
            ).throws(GameDoesNotExistError);
        });

        it('throws IncorrectGameStatusError if game is not started', () => {
            const oldGame = constructDiagonalGame(3);

            expect(
                () => executeMovePlayerCommand(command, oldGame)
            ).throws(IncorrectGameStatusError);
        });

        it('throws PlayerNotFoundError if player is not found', () => {
            const incorrectCommand: MovePlayerCommand = {
                commandType: CommandType.MOVE_PLAYER,
                direction: Direction.SOUTH,
                playerId: 9,
            };

            const oldGame = constructDiagonalGame(3).start();

            expect(
                () => executeMovePlayerCommand(incorrectCommand, oldGame)
            ).throws(PlayerNotFoundError);
        });

        it('throws IncorrectPlayerStatusError if player is not playing', () => {
            const oldGame = constructDiagonalGame(3).start();

            expect(
                () => executeMovePlayerCommand(command, oldGame)
            ).throws(IncorrectPlayerStatusError);
        });

        it('throws PlayerHasNoCoordinatesError if player is misconfigured', () => {
            const oldGame = constructDiagonalGame(3).start();

            const oldPlayer = oldGame.getPlayer(0) as Player;
            const newPlayer = oldPlayer.makePlaying().setCoordinates(undefined);

            const newGame = oldGame.mergeEntities([], [newPlayer]);

            expect(
                () => executeMovePlayerCommand(command, newGame)
            ).throws(PlayerHasNoCoordinatesError);
        });

        it('throws CellNotFoundError if the cell does not exist', () => {
            const oldGame = constructDiagonalGame(3).start();

            const oldPlayer = oldGame.getPlayer(0) as Player;
            const newPlayer = oldPlayer.makePlaying().setCoordinates([4, 4]);

            const newGame = oldGame.mergeEntities([], [newPlayer]);

            expect(
                () => executeMovePlayerCommand(command, newGame)
            ).throws(CellNotFoundError);
        });

        it('throws CellNotAccessibleError if the cell is not accessible', () => {
            const oldGame = constructDiagonalGame(3).start();

            const oldPlayer = oldGame.getPlayer(0) as Player;
            const newPlayer = oldPlayer.makePlaying();

            const oldCell = oldGame.getCellByCoordinates([0, 1]) as Cell;
            const newCell = oldCell.setAccessible(false);

            const newGame = oldGame.mergeEntities([newCell], [newPlayer]);

            expect(
                () => executeMovePlayerCommand(command, newGame)
            ).throws(CellNotAccessibleError);
        });

        it('runs thru movePlayerIntoOwnerlessCell path', () => {
            const oldGame = constructDiagonalGame(3).start();

            const oldPlayer = oldGame.getPlayer(0) as Player;
            const newPlayer = oldPlayer.makePlaying();

            const newGame = oldGame.mergeEntities([], [newPlayer]);

            const game = executeMovePlayerCommand(command, newGame);

            const player = game.getPlayer(0) as Player;
            
            expect(player.coordinates).to.deep.equal([0, 1]);
        });

        it('destroy the oldPlayer if numberOfOtherPlayerCells > numberOfOldPlayerCells', () => {
            const oldGame = constructDiagonalGame(2).start();

            const oldPlayer = oldGame.getPlayer(0) as Player;
            const newPlayer = oldPlayer.makePlaying();

            const oldOtherPlayer = oldGame.getPlayer(1) as Player;
            const newOtherPlayer = oldOtherPlayer.makePlaying().setCoordinates([0, 1]);

            const newPlayers = [ newPlayer, newOtherPlayer];

            const newCells = oldGame.getCells()
                .filter(cell => cell.ownerId === undefined)
                .map(cell => cell.setOwnerId(1));

            const newGame = oldGame.mergeEntities(newCells, newPlayers);

            const game = executeMovePlayerCommand(command, newGame);

            expect(game.status).to.equal(GameStatus.FINISHED);

            const player = game.getPlayer(0) as Player;
            expect(player.status).to.equal(PlayerStatus.LOST);

            const winningPlayer = game.getPlayer(1) as Player;
            expect(winningPlayer.status).to.equal(PlayerStatus.WON);
        });

        it('destroy both players if numberOfOtherPlayerCells === numberOfOldPlayerCells', () => {
            const oldGame = constructDiagonalGame(2).start();

            const oldPlayer = oldGame.getPlayer(0) as Player;
            const newPlayer = oldPlayer.makePlaying().setCoordinates([0, 0]);

            const oldOtherPlayer = oldGame.getPlayer(1) as Player;
            const newOtherPlayer = oldOtherPlayer.makePlaying().setCoordinates([0, 1]);

            const newPlayers = [ newPlayer, newOtherPlayer];

            const newCells = oldGame.getCells()
                .map(cell => cell.setOwnerId(cell.coordinates[1]));

            const newGame = oldGame.mergeEntities(newCells, newPlayers);

            const game = executeMovePlayerCommand(command, newGame);

            expect(game.status).to.equal(GameStatus.FINISHED);

            const player = game.getPlayer(0) as Player;
            expect(player.status).to.equal(PlayerStatus.LOST);

            const winningPlayer = game.getPlayer(1) as Player;
            expect(winningPlayer.status).to.equal(PlayerStatus.LOST);
        });

        it('destroy the other players if numberOfOtherPlayerCells < numberOfOldPlayerCells', () => {
            const oldGame = constructDiagonalGame(2).start();

            const oldPlayer = oldGame.getPlayer(0) as Player;
            const newPlayer = oldPlayer.makePlaying().setCoordinates([0, 0]);

            const oldOtherPlayer = oldGame.getPlayer(1) as Player;
            const newOtherPlayer = oldOtherPlayer.makePlaying().setCoordinates([0, 1]);

            const newPlayers = [ newPlayer, newOtherPlayer];

            const newCells = oldGame.getCells()
                .filter(cell => cell.ownerId === undefined)
                .map(cell => cell.setOwnerId(0));

            const newGame = oldGame.mergeEntities(newCells, newPlayers);

            const game = executeMovePlayerCommand(command, newGame);

            expect(game.status).to.equal(GameStatus.FINISHED);

            const player = game.getPlayer(0) as Player;
            expect(player.status).to.equal(PlayerStatus.WON);

            const winningPlayer = game.getPlayer(1) as Player;
            expect(winningPlayer.status).to.equal(PlayerStatus.LOST);
        });
    });

    describe('destroyOldPlayer', () => {
        it('makes a player lose', () => {
            const oldGame = constructDiagonalGame(3);
            const oldPlayer = oldGame.getPlayer(0) as Player;
            const newGame = destroyOldPlayer(oldPlayer, oldGame);

            expect(newGame).not.to.be.undefined;

            const newPlayer = newGame.getPlayer(0) as Player;
            expect(newPlayer.status).to.equal(PlayerStatus.LOST);
        });
    });

    describe('destroyBothPlayers', () => {
        it('makes both players lose and frees the cell', () => {
            const oldGame = constructDiagonalGame(2);

            const oldPlayers = oldGame.getPlayers();

            const oldPlayer1 = oldPlayers[0];
            const oldPlayer2 = oldPlayers[1];

            const { coordinates } = oldPlayer2;

            const oldCell = oldGame.getCellByCoordinates(coordinates as Coordinates) as Cell;

            const newGame = destroyBothPlayers(oldPlayer1, oldPlayer2, oldCell, oldGame);

            expect(newGame).not.to.be.undefined;
            expect(newGame.filterPlayers(p => p.status === PlayerStatus.LOST).length).to.equal(2);

            const newCell = newGame.getCellByCoordinates(coordinates as Coordinates) as Cell;

            expect(newCell.ownerId).to.be.undefined;
        });
    });

    describe('destroyOtherPlayerAndMoveOldPlayer', () => {
        it('destroys one player and move the other', () => {
            const oldGame = constructDiagonalGame(2);

            const oldPlayers = oldGame.getPlayers();
            const oldPlayer1 = oldPlayers[0];
            const oldPlayer2 = oldPlayers[1];

            const { coordinates } = oldPlayer2;

            const oldCell = oldGame.getCellByCoordinates(coordinates as Coordinates) as Cell;

            const newGame = destroyOtherPlayerAndMoveOldPlayer(oldPlayer1, oldPlayer2, oldCell, oldGame);

            const newPlayers = newGame.getPlayers();
            const newPlayer1 = newPlayers[0];
            const newPlayer2 = newPlayers[1];

            expect(newPlayer1.status).to.be.equal(PlayerStatus.CREATED);
            expect(newPlayer2.status).to.be.equal(PlayerStatus.LOST);

            const newCell = newGame.getCellByCoordinates(coordinates as Coordinates) as Cell;

            expect(newCell.ownerId).to.be.equal(newPlayer1.id);
        });
    });

    describe('movePlayerIntoOwnerlessCell', () => {
        it('moves the player', () => {
            const coordinates: Coordinates = [0, 1];

            const oldGame = constructDiagonalGame(2);
            const oldPlayer = oldGame.getPlayer(0) as Player;
            const oldCell = oldGame.getCellByCoordinates(coordinates) as Cell;

            const newGame = movePlayerIntoOwnerlessCell(oldPlayer, oldCell, oldGame);

            const newPlayer = newGame.getPlayer(0) as Player;

            expect(newPlayer.status).to.be.equal(PlayerStatus.CREATED);
            expect(newPlayer.coordinates).to.be.deep.equal([0, 1]);

            const newCell = newGame.getCellByCoordinates(coordinates) as Cell;
            
            expect(newCell.ownerId).to.be.equal(newPlayer.id);
        });
    });
});