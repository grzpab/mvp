import 'mocha';
import { expect } from 'chai';

import { executeJoinGameCommand } from '../../../src/executables/executeJoinGameCommand';
import { CommandType } from '../../../src/commands/commandTypes';
import { JoinGameCommand } from '../../../src/commands/incomingCommands';
import {
    GameDoesNotExistError,
    IncorrectGameStatusError,
    PlayerAlreadyExistsError,
    CellAlreadyOwnerError,
    CellNotFoundError,
} from '../../../src/errors';
import { constructDiagonalGame } from '../../helpers/gameHelpers';
import { GameStatus } from '../../../src/types/game';
import { PlayerStatus, Player } from '../../../src/types/players';
import { Cell } from '../../../src/types/cells';

describe('/executables/executeJoinGameCommand.ts', () => {
    describe('executeJoinGameCommand', () => {
        const command: JoinGameCommand = {
            commandType: CommandType.JOIN_GAME,
            playerId: 3,
            playerCoordinates: [0, 2],
        };

        const oldGame = constructDiagonalGame(3);

        it('throws GameDoesNotExistError if no game defined', () => {
            expect(
                () => executeJoinGameCommand(command, undefined)
            ).to.throw(GameDoesNotExistError);
        });

        it('throws IncorrectGameStatusError if the game has been started', () => {
            expect(
                () => executeJoinGameCommand(command, oldGame.start())
            ).to.throw(IncorrectGameStatusError);
        });

        it('throws PlayerAlreadyExistsError if the playerId has been used', () => {
            const newCommand = Object.assign({}, command, { playerId: 2});

            expect(
                () => executeJoinGameCommand(newCommand, oldGame)
            ).to.throw(PlayerAlreadyExistsError);
        });

        it('throws CellAlreadyOwnerError if the cell is owned', () => {
            const newCommand = Object.assign({}, command, { playerCoordinates: [ 1, 1]});

            expect(
                () => executeJoinGameCommand(newCommand, oldGame)
            ).to.throw(CellAlreadyOwnerError);
        });

        it('throws CellNotFoundError if the cell does not exist', () => {
            const newCommand = Object.assign({}, command, { playerCoordinates: [ 3, 3]});

            expect(
                () => executeJoinGameCommand(newCommand, oldGame)
            ).to.throw(CellNotFoundError);
        });

        it('creates a new version of game', () => {
            const newGame = executeJoinGameCommand(command, oldGame);

            expect(newGame.status).to.be.equal(GameStatus.INITIATED);
            expect(newGame.size).to.be.deep.equal([3, 3]);

            expect(newGame.getPlayers().map(player => player.id).sort()).to.be.deep.equal([0, 1, 2, 3]);

            const newPlayer = newGame.getPlayer(3) as Player;
            expect(newPlayer.id).to.be.equal(3);
            expect(newPlayer.status).to.be.equal(PlayerStatus.CREATED);
            expect(newPlayer.coordinates).to.be.deep.equal([0, 2]);

            const newCell = newGame.getCellByCoordinates([0, 2]) as Cell;
            expect(newCell.ownerId).to.be.equal(3);
        });
    });
});