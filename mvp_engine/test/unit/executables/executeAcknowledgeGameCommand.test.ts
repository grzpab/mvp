import 'mocha';
import { expect } from 'chai';

import { CommandType } from '../../../src/commands/commandTypes';
import {
    executeAcknowledgeGameCommand
} from '../../../src/executables/executeAcknowledgeGameCommand'; 
import { AcknowledgeGameCommand } from '../../../src/commands/incomingCommands';
import {
    GameDoesNotExistError,
    IncorrectGameStatusError,
    PlayerNotFoundError,
    IncorrectPlayerStatusError
} from '../../../src/errors';
import { get2x2Game, addPlayer0, acknowledgePlayer0 } from '../../helpers/gameHelpers';
import { GameStatus } from '../../../src/types/game';
import { PlayerStatus } from '../../../src/types/players';

describe('/executables/executeAcknowledgeGameCommand.ts', () => {
    describe('executeAcknowledgeGameCommand', () => {
        const command: AcknowledgeGameCommand = {
            commandType: CommandType.ACKNOWLEDGE_GAME,
            playerId: 0
        };

        it('throws a GameDoesNotExistError if a game does not exist', () => {
            expect(
                () => executeAcknowledgeGameCommand(command, undefined)
            ).to.throw(GameDoesNotExistError);
        });

        it('throws a IncorrectGameStatusError if a game is after initialization', () => {
            const game = get2x2Game().start();

            expect(
                () => executeAcknowledgeGameCommand(command, game)
            ).to.throw(IncorrectGameStatusError);
        });

        it('throws a PlayerNotFoundError if there is no player found', () => {
            const game = get2x2Game();

            expect(
                () => executeAcknowledgeGameCommand(command, game)
            ).to.throw(PlayerNotFoundError);
        });

        it('throws a IncorrectPlayerStatusError if the player status is incorrect', () => {
            const game = acknowledgePlayer0(
                addPlayer0(
                    get2x2Game()
                )
            );
            
            expect(
                () => executeAcknowledgeGameCommand(command, game)
            ).to.throw(IncorrectPlayerStatusError);
        });

        it('returns a new Game instance', (done) => {
            const oldGame = addPlayer0(get2x2Game());
            const newGame = executeAcknowledgeGameCommand(command, oldGame);

            expect(newGame.status).to.equal(GameStatus.INITIATED);
            
            const player = newGame.getPlayer(0);

            expect(player).not.to.be.undefined;

            if (player === undefined) {
                return;
            }

            expect(player.status).to.equal(PlayerStatus.ACKNOWLEDGED);

            done();
        });
    });

});