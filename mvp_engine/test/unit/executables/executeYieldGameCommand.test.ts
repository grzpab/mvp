import 'mocha';
import { expect } from 'chai';
import { YieldGameCommand } from '../../../src/commands/incomingCommands';
import { CommandType } from '../../../src/commands/commandTypes';
import { constructDiagonalGame } from '../../helpers/gameHelpers';
import {
    GameDoesNotExistError,
    PlayerNotFoundError,
    IncorrectPlayerStatusError,
    PlayerHasNoCoordinatesError,
    CellNotFoundError,
} from '../../../src/errors';
import { executeYieldGameCommand } from '../../../src/executables/executeYieldGameCommand';
import { Player, PlayerStatus } from '../../../src/types/players';
import { GameStatus } from '../../../src/types/game';

describe('/executables/executeYieldGameCommand.ts', () => {
    describe('executeYieldGameCommand', () => {
        const command: YieldGameCommand = {
            commandType: CommandType.YIELD_GAME,
            playerId: 0,
        };

        it('throws GameDoesNotExistError if game is undefined', () => {
            const oldGame = undefined;
            
            expect(
                () => executeYieldGameCommand(command, oldGame)
            ).throws(GameDoesNotExistError);
        });

        it('throws GameDoesNotExistError if game is not started', () => {
            const oldGame = constructDiagonalGame(3);
            
            expect(
                () => executeYieldGameCommand(command, oldGame)
            ).throws(GameDoesNotExistError);
        });

        it('throws PlayerNotFoundError if player is not started', () => {
            const oldGame = constructDiagonalGame(3).start();

            const incorrectCommand: YieldGameCommand = {
                commandType: CommandType.YIELD_GAME,
                playerId: 9,
            };
            
            expect(
                () => executeYieldGameCommand(incorrectCommand, oldGame)
            ).throws(PlayerNotFoundError);
        });

        it('throws IncorrectPlayerStatusError if player is not playing', () => {
            const oldGame = constructDiagonalGame(3).start();
            
            expect(
                () => executeYieldGameCommand(command, oldGame)
            ).throws(IncorrectPlayerStatusError);
        });

        it('throws PlayerHasNoCoordinatesError if player has no coordinates', () => {
            const oldGame = constructDiagonalGame(3).start();

            const player = oldGame.getPlayer(0) as Player;
            const newPlayer = player.makePlaying().setCoordinates(undefined);

            const newGame = oldGame.mergeEntities([], [ newPlayer ]);
            
            expect(
                () => executeYieldGameCommand(command, newGame)
            ).throws(PlayerHasNoCoordinatesError);
        });

        it('throws CellNotFoundError if player has incorrect coordinates', () => {
            const oldGame = constructDiagonalGame(3).start();

            const player = oldGame.getPlayer(0) as Player;
            const newPlayer = player.makePlaying().setCoordinates([ 4, 4 ]);

            const newGame = oldGame.mergeEntities([], [ newPlayer ]);
            
            expect(
                () => executeYieldGameCommand(command, newGame)
            ).throws(CellNotFoundError);
        });

        it('yields the player and finished the game', () => {
            const oldGame = constructDiagonalGame(2).start();

            const newPlayers = oldGame.getPlayers().map(p => p.makePlaying());

            const newGame = oldGame.mergeEntities([], newPlayers);
            
            const game = executeYieldGameCommand(command, newGame);

            expect(game.status).to.equal(GameStatus.FINISHED);

            const playerWhoLost = game.getPlayer(0) as Player;
            expect(playerWhoLost.status).to.equal(PlayerStatus.LOST);

            const playerWhoWon = game.getPlayer(1) as Player;
            expect(playerWhoWon.status).to.equal(PlayerStatus.WON);
        });
    });
});