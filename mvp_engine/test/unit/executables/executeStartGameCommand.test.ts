import 'mocha';
import { expect } from 'chai';

import { StartGameCommand } from '../../../src/commands/incomingCommands';
import { CommandType } from '../../../src/commands/commandTypes';
import { executeStartGameCommand } from '../../../src/executables/executeStartGameCommand';
import { GameDoesNotExistError, IncorrectGameStatusError } from '../../../src/errors';
import { constructDiagonalGame } from '../../helpers/gameHelpers';
import { PlayerStatus } from '../../../src/types/players';
import { GameStatus } from '../../../src/types/game';

describe('/executables/executeStartGameCommand.ts', () => {
    describe('executeStartGameCommand', () => {
        const command: StartGameCommand = {
            commandType: CommandType.START_GAME,
        };

        it('throws GameDoesNotExistError if game is undefined', () => {
            const oldGame = undefined;
            
            expect(
                () => executeStartGameCommand(command, oldGame)
            ).throws(GameDoesNotExistError);
        });

        it('throws IncorrectGameStatusError if game status if incorrect', () => {
            const oldGame = constructDiagonalGame(3).start();
            
            expect(
                () => executeStartGameCommand(command, oldGame)
            ).throws(IncorrectGameStatusError);
        });

        it('removes all unknowledged players and finishes the game', () => {
            const oldGame = constructDiagonalGame(2);
            
            const newGame = executeStartGameCommand(command, oldGame);

            expect(newGame.status).to.equal(GameStatus.FINISHED);
            expect(newGame.filterPlayers(p => p.status === PlayerStatus.PLAYING).length).to.equal(0);
        });

        it('starts the game', () => {
            const oldGame = constructDiagonalGame(2);

            const newPlayers = oldGame.getPlayers().map(p => p.acknowledge());
            const newGame = oldGame.mergeEntities([], newPlayers);
            
            const game = executeStartGameCommand(command, newGame);

            expect(game.status).to.equal(GameStatus.STARTED);
            expect(game.filterPlayers(p => p.status === PlayerStatus.PLAYING).length).to.equal(2);
        });
    });
});