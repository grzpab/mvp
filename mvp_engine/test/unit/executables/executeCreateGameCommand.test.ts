import 'mocha';
import { expect } from 'chai';

import { CreateGameCommand } from '../../../src/commands/incomingCommands';
import { CommandType } from '../../../src/commands/commandTypes';
import { constructCells, constructCoordinatesList } from '../../helpers/gameHelpers';
import { Coordinates } from '../../../src/types/coordinates';
import {
    executeCreateGameCommand
} from '../../../src/executables/executeCreateGameCommand';
import { CoordinatesOutOfBoundsError } from '../../../src/errors';
import { GameStatus } from '../../../src/types/game';

describe('/executables/executeCreateGameCommand.ts', () => {
    const alwaysTruePredicate = () => true;
    const getCellOwnerId = ([x, y]: Coordinates) => x === y ? x : undefined;
    
    describe('executeCreateGameCommand', () => {
        it('throws CoordinatesOutOfBoundsError if the player coordinates are not in bounds', () => {
            const gameSize: Coordinates = [3, 3];
            const coordinatesList = constructCoordinatesList(gameSize);

            const command: CreateGameCommand = {
                commandType: CommandType.CREATE_GAME,
                playerId: 0,
                playerCoordinates: [4, 1],
                size: gameSize,
                cells: constructCells(
                    gameSize,
                    coordinatesList,
                    alwaysTruePredicate,
                    getCellOwnerId,
                )
            };

            expect(() => executeCreateGameCommand(command)).to.throw(CoordinatesOutOfBoundsError);
        });

        it('creates a new Game', () => {
            const gameSize: Coordinates = [3, 3];
            const coordinatesList = constructCoordinatesList(gameSize);

            const command: CreateGameCommand = {
                commandType: CommandType.CREATE_GAME,
                playerId: 0,
                playerCoordinates: [1, 1],
                size: gameSize,
                cells: constructCells(
                    gameSize,
                    coordinatesList,
                    alwaysTruePredicate,
                    getCellOwnerId,
                )
            };

            const game = executeCreateGameCommand(command);

            expect(game.size).to.be.equal(gameSize);
            expect(game.status).to.be.equal(GameStatus.INITIATED);
            expect(game.getCells().length).to.be.equal(9);
            expect(game.getPlayers().length).to.be.equal(1);
        });
    });
});