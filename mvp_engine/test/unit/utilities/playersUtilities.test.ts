import 'mocha';
import { expect } from 'chai';

import { Player } from '../../../src/types/players';
import {
    createNewPlayerMap,
    mergeNewPlayers
} from '../../../src/utilities/playerUtilities';

describe('/utilities/playerUtilities.ts', () => {
    describe('createNewPlayerMap', () => {
        it('returns an empty map', () => {
            const map = createNewPlayerMap();

            expect(map.size()).to.equal(0);
            expect(map.values().length).to.equal(0);
        });

        it('maps a player by its ID', () => {
            const map = createNewPlayerMap();

            expect(map.size()).to.equal(0);
            expect(map.values().length).to.equal(0);

            const player = Player.create(10, [2, 2]);
            map.put(player);

            expect(map.size()).to.equal(1);

            expect(map.values()).to.deep.equal([player]);

            const fetchedPlayer = map.get(10);

            expect(fetchedPlayer).not.to.be.undefined;

            expect(fetchedPlayer).to.equal(player);
        });
    });

    describe('mergeNewPlayers', () => {
        it('returns the same map if no array given', () => {
            const oldPlayerMap = createNewPlayerMap();
    
            const newPlayerMap = mergeNewPlayers(
                oldPlayerMap,
                []
            );
    
            expect(newPlayerMap).to.equal(newPlayerMap);
        });
    
        it('returns the map with array elements if empty map given', () => {
            const oldPlayerMap = createNewPlayerMap();
    
            const newPlayerMap = mergeNewPlayers(
                oldPlayerMap,
                [
                    Player.create(1, [0, 0])
                ]
            );
    
            expect(newPlayerMap.size()).to.equal(1);
        });
    
        it('returns the map with elements from both array and the old map', () => {
            const oldPlayerMap = createNewPlayerMap();
            oldPlayerMap.put(
                Player.create(1, [0, 0])
            );
    
            const newPlayerMap = mergeNewPlayers(
                oldPlayerMap,
                [
                    Player.create(2, [1, 1])
                ]
            );
    
            expect(newPlayerMap.size()).to.equal(2);
        });
    
        it('returns the map with the replaced element', () => {
            const oldPlayerMap = createNewPlayerMap();
            oldPlayerMap.put(
                Player.create(1, [0, 1])
            );
    
            const player = Player.create(1, [2, 3]).makeLose();
    
            const newPlayerMap = mergeNewPlayers(
                oldPlayerMap,
                [
                    player,
                ]
            );
    
            expect(newPlayerMap.size()).to.equal(1);
    
            expect(newPlayerMap.get(1)).to.equal(player);
        });
    });
    
});