import 'mocha';
import { expect } from 'chai';

import { Cell } from '../../../src/types/cells';
import {
    createNewCellMap,
    areCoordinatesInBounds,
} from '../../../src/utilities/cellUtilities';

describe('/utilities/cellUtilities.ts', () => {
    describe('createNewCellMap', () => {
        it('returns an empty map', () => {
            const map = createNewCellMap();

            expect(map.size()).to.equal(0);
            expect(map.values().length).to.equal(0);
        });

        it('maps a cell by its ID', () => {
            const map = createNewCellMap();

            expect(map.size()).to.equal(0);
            expect(map.values().length).to.equal(0);

            const cell = new Cell(15, [3, 3], true, undefined);
            map.put(cell);

            expect(map.size()).to.equal(1);

            expect(map.values()).to.deep.equal([cell]);

            const fetchedCell = map.get(15);

            expect(fetchedCell).not.to.be.undefined;

            expect(fetchedCell).to.equal(cell);
        });
    });

    describe('areCoordinatesInBounds', () => {
        it('returns false if it is not in bounds', () => {
            expect(areCoordinatesInBounds([5, 5], [-1, -1])).to.be.false;
            expect(areCoordinatesInBounds([5, 5], [ 1, -1])).to.be.false;
            expect(areCoordinatesInBounds([5, 5], [-1,  1])).to.be.false;

            expect(areCoordinatesInBounds([5, 5], [5, 5])).to.be.false;
            expect(areCoordinatesInBounds([5, 5], [6, 4])).to.be.false;
            expect(areCoordinatesInBounds([5, 5], [4, 6])).to.be.false;
        });

        it('returns true if it is in bounds', () => {
            expect(areCoordinatesInBounds([5, 5], [0, 0])).to.be.true;
            expect(areCoordinatesInBounds([5, 5], [0, 1])).to.be.true;
            expect(areCoordinatesInBounds([5, 5], [1, 0])).to.be.true;
            expect(areCoordinatesInBounds([5, 5], [4, 4])).to.be.true;
            expect(areCoordinatesInBounds([5, 5], [4, 3])).to.be.true;
            expect(areCoordinatesInBounds([5, 5], [3, 4])).to.be.true;
            expect(areCoordinatesInBounds([5, 5], [2, 2])).to.be.true;
        });
    });
});