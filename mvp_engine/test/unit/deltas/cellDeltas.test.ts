import 'mocha';
import { expect } from 'chai';

import { getCellDelta } from '../../../src/deltas/cellDeltas';
import { getAccessibleUnoccupiedCell } from '../../helpers/gameHelpers';
import { CellIdMismatchError, CellDeltaRuntimeError } from '../../../src/errors';

describe('/deltas/cellDeltas.ts', () => {
    describe('getCellDelta', () => {
        it('returns undefined if both arguments undefined', () => {
            const cellDelta = getCellDelta(undefined)(undefined);
            expect(cellDelta).to.be.undefined;
        });

        it('returns undefined if both cells are the same (by reference)', () => {
            const cell = getAccessibleUnoccupiedCell([2, 2], [1, 1]);

            const cellDelta = getCellDelta(cell)(cell);
            expect(cellDelta).to.be.undefined;
        });

        it('returns undefined if both cells are the same (by value)', () => {
            const oldCell = getAccessibleUnoccupiedCell([2, 2], [1, 1]);
            const newCell = getAccessibleUnoccupiedCell([2, 2], [1, 1]);

            const cellDelta = getCellDelta(oldCell)(newCell);
            expect(cellDelta).to.be.undefined;
        });

        it('returns data from new cell if the original one was undefined', () => {
            const oldCell = undefined;
            const newCell = getAccessibleUnoccupiedCell([2, 2], [1, 1]);

            const cellDelta = getCellDelta(oldCell)(newCell);
            expect(cellDelta).to.be.deep.equal({
                id: 3,
                newOwnerId: undefined,
                ownerIdChanged: true,
            });
        });

        it('returns data with the new owner ID if the original one was undefined', () => {
            const oldCell = getAccessibleUnoccupiedCell([2, 2], [1, 1]);
            const newCell = oldCell.setOwnerId(2);

            const cellDelta = getCellDelta(oldCell)(newCell);
            expect(cellDelta).to.be.deep.equal({
                id: 3,
                newOwnerId: 2,
                ownerIdChanged: true,
            });
        });

        it('throws an error if cell IDs are mismatches', () => {
            const oldCell = getAccessibleUnoccupiedCell([2, 2], [1, 1]);
            const newCell = getAccessibleUnoccupiedCell([2, 2], [1, 0]);

            expect(() => getCellDelta(oldCell)(newCell)).to.throw(CellIdMismatchError);
        });

        it('throws an error if new cell is undefined', () => {
            const oldCell = getAccessibleUnoccupiedCell([2, 2], [1, 1]);
            const newCell = undefined;

            expect(() => getCellDelta(oldCell)(newCell)).to.throw(CellDeltaRuntimeError);
        });
    });
});