import 'mocha';
import { expect } from 'chai';

import { getPlayerDelta } from '../../../src/deltas/playerDeltas';
import { Player } from '../../../src/types/players';
import { PlayerIdMismatchError, PlayerDeltaRuntimeError } from '../../../src/errors';

describe('/deltas/playerDeltas.ts', () => {
    describe('getPlayerDelta', () => {
        it('returns undefined if both arguments undefined', () => {
            const playerDelta = getPlayerDelta(undefined)(undefined);
            expect(playerDelta).to.be.undefined;
        });

        it('returns undefined if both players are the same (by reference)', () => {
            const player = Player.create(0, [1, 1]);

            const playerDelta = getPlayerDelta(player)(player);
            expect(playerDelta).to.be.undefined;
    });

        it('returns a structure if both players are the same (by value)', () => {
            const oldPlayer = Player.create(0, [1, 1]);
            const newPlayer = Player.create(0, [1, 1]);

            const playerDelta = getPlayerDelta(oldPlayer)(newPlayer);
            expect(playerDelta).to.be.undefined;
        });

        it('returns data from new player if the original one was undefined', () => {
            const oldPlayer = undefined;
            const newPlayer = Player.create(0, [1, 1]);

            const playerDelta = getPlayerDelta(oldPlayer)(newPlayer);

            expect(playerDelta).to.be.deep.equal({
                id: 0,
                coordinatesChanged: true,
                newCoordinates: [1, 1],
                statusChanged: true,
                newStatus: 'CREATED',
            });
        });

        it('returns new coordinates if a change was spotted', () => {
            const oldPlayer = Player.create(0, [1, 1]);
            const newPlayer = oldPlayer.setCoordinates([2, 2]);

            const playerDelta = getPlayerDelta(oldPlayer)(newPlayer);

            expect(playerDelta).to.be.deep.equal({
                id: 0,
                coordinatesChanged: true,
                newCoordinates: [2, 2],
                statusChanged: false,
                newStatus: 'CREATED',
            });
        });

        it('returns new status if a change was spotted', () => {
            const oldPlayer = Player.create(0, [1, 1]);
            const newPlayer = oldPlayer.makeWin();

            const playerDelta = getPlayerDelta(oldPlayer)(newPlayer);

            expect(playerDelta).to.be.deep.equal({
                id: 0,
                coordinatesChanged: false,
                newCoordinates: [1, 1],
                statusChanged: true,
                newStatus: 'WON',
            });
        });

        it('throws an error if player IDs are mismatches', () => {
            const oldPlayer = Player.create(0, [1, 1]);
            const newPlayer = Player.create(2, [1, 1]);

            expect(() => getPlayerDelta(oldPlayer)(newPlayer)).to.throw(PlayerIdMismatchError);
        });

        it('throws an error if new player cell is undefined', () => {
            const oldPlayer = Player.create(0, [1, 1]);
            const newPlayer = undefined;

            expect(() => getPlayerDelta(oldPlayer)(newPlayer)).to.throw(PlayerDeltaRuntimeError);
        });
    });
});