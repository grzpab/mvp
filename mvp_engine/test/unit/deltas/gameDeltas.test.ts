import 'mocha';
import { expect } from 'chai';
import { constructDiagonalGame } from '../../helpers/gameHelpers';
import { getGameDelta } from '../../../src/deltas/gameDeltas';
import { GameDelta, GameStatus } from '../../../src/types/game';
import { Player, PlayerStatus } from '../../../src/types/players';
import { Cell } from '../../../src/types/cells';
import { GameDeltaRuntimeError } from '../../../src/errors';

describe('/deltas/gameDeltas.ts', () => {
    describe('getGameDelta', () => {
        it('returns undefined if oldGame is newGame', () => {
            const oldGame = constructDiagonalGame(3);
            const newGame = oldGame;

            const delta = getGameDelta(oldGame, newGame);

            expect(delta).to.be.undefined;
        });

        it('returns a delta if a game has been just created', () => {
            const oldGame = undefined;
            const newGame = constructDiagonalGame(3);

            const delta = getGameDelta(oldGame, newGame) as GameDelta;

            expect(delta).not.to.be.undefined;
            expect(delta.newStatus).to.equal(GameStatus.INITIATED);
            expect(delta.statusChanged).to.be.true;
            expect(delta.cellDeltas.length).to.equal(9);
            expect(delta.playerDeltas.length).to.equal(3);
        });

        it('returns a delta if games are different', () => {
            const oldGame = constructDiagonalGame(3);

            const oldCell = oldGame.getCellByCoordinates([0, 0]) as Cell;
            const newCell = oldCell.setOwnerId(undefined);

            const oldPlayer = oldGame.getPlayer(0) as Player;
            const newPlayer = oldPlayer.makeLose();

            const newGame = oldGame.mergeEntities(
                [ newCell ],
                [ newPlayer ]
            );

            const delta = getGameDelta(oldGame, newGame) as GameDelta;

            expect(delta).not.to.be.undefined;

            expect(delta.newStatus).to.equal(GameStatus.INITIATED);
            expect(delta.statusChanged).to.be.false;

            expect(delta.cellDeltas.length).to.equal(1);
            expect(delta.playerDeltas.length).to.equal(1);

            const cellDelta = delta.cellDeltas[0];

            expect(cellDelta.id).to.equal(0);
            expect(cellDelta.newOwnerId).to.be.undefined;
            expect(cellDelta.ownerIdChanged).to.be.true;

            const playerDelta = delta.playerDeltas[0];
            expect(playerDelta.id).to.equal(0);
            expect(playerDelta.newStatus).to.equal(PlayerStatus.LOST);
            expect(playerDelta.newCoordinates).to.deep.equal([0, 0]);
            expect(playerDelta.statusChanged).to.be.true;
            expect(playerDelta.coordinatesChanged).to.be.false;
        });

        it('throws an Error', () => {
            const oldGame = constructDiagonalGame(3);
            const newGame = undefined;

            expect(
                () => getGameDelta(oldGame, newGame)
            ).to.throw(GameDeltaRuntimeError);
        });
    });
});