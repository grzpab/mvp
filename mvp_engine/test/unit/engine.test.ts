import 'mocha';
import { expect } from 'chai';

import { Engine, ExecuteIncomingCommands } from '../../src/engine';
import { IncomingCommands } from '../../src/commands/incomingCommands';
import { OutgoingCommands } from '../../src/commands/outgoingCommands';

describe('engine.ts', () => {
    it('Engine::constructor registers callbacks', (done) => {
        const incomingCommands: IncomingCommands = [];

        const incomingCommandCallback = (executeIncommingCommands: ExecuteIncomingCommands) => {
            executeIncommingCommands(incomingCommands);
        };

        const outgoingCommandCallback = (outgoingCommands: OutgoingCommands) => {
            done();
        };
        
        const engine = new Engine(
            incomingCommandCallback,
            outgoingCommandCallback,
        );

        expect(engine.game).to.be.undefined;
    });
});