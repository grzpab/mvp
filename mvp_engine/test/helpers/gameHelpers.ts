import { Game } from '../../src/types/game';
import { Cell, CellMap } from '../../src/types/cells';
import { Coordinates } from '../../src/types/coordinates'; 
import { getCellId, createNewCellMap } from '../../src/utilities/cellUtilities';
import { createNewPlayerMap } from '../../src/utilities/playerUtilities';
import { Player, PlayerMap } from '../../src/types/players';

export function getAccessibleUnoccupiedCell(
    gameSize: Coordinates,
    coordinates: Coordinates
): Cell {
    const cellId = getCellId(gameSize, coordinates);

    return new Cell(
        cellId,
        coordinates,
        true,
        undefined,
    );
}

export function get2x2Game() {
    const gameSize: Coordinates = [2, 2];

    const coordinatesList: ReadonlyArray<Coordinates> = [
        [0, 0],
        [0, 1],
        [1, 0],
        [1, 1]
    ];

    const cells = coordinatesList.map(coordinates => getAccessibleUnoccupiedCell(
        gameSize,
        coordinates
    ));

    const cellMap = createNewCellMap().putArray(cells);
    const playerMap = createNewPlayerMap();

    return Game.initiate(gameSize, cellMap, playerMap);
}

export function addPlayer0(oldGame: Game) {
    const coordinates: Coordinates = [0, 0];
    const newPlayer = Player.create(0, coordinates);
    
    const oldCell = oldGame.getCellByCoordinates(coordinates);
    if (oldCell === undefined) {
        throw new ReferenceError();
    }

    const newCell = oldCell.setOwnerId(newPlayer.id);

    return oldGame.mergeEntities([newCell], [newPlayer]);
}

export function acknowledgePlayer0(oldGame: Game) {
    const oldPlayer = oldGame.getPlayer(0);

    if (oldPlayer === undefined) {
        throw new ReferenceError();
    }

    const newPlayer = oldPlayer.acknowledge();

    return oldGame.mergeEntities([], [newPlayer]);
}

export function constructCoordinatesList(gameSize: Coordinates): ReadonlyArray<Coordinates> {
    const coordinatesList: Array<Coordinates> = [];
    
    for (let i = 0; i < gameSize[0]; i++) {
        for (let j = 0; j < gameSize[1]; j++) {
            coordinatesList.push([i, j]);
        }
    }

    return coordinatesList;
}

export function constructCells(
    gameSize: Coordinates,
    coordinatesList: ReadonlyArray<Coordinates>,
    isCellAccessible: (coordinates: Coordinates) => boolean,
    getCellOwnerId: (coordinates: Coordinates) => number | undefined,
): ReadonlyArray<Cell> {
    return coordinatesList.map(coordinates => new Cell(
        getCellId(gameSize, coordinates),
        coordinates,
        isCellAccessible(coordinates),
        getCellOwnerId(coordinates)
    ));
}

export function constructCellMap(
    gameSize: Coordinates,
    coordinatesList: ReadonlyArray<Coordinates>,
    isCellAccessible: (coordinates: Coordinates) => boolean,
    getCellOwnerId: (coordinates: Coordinates) => number | undefined,
): CellMap {
    const cells = constructCells(gameSize, coordinatesList, isCellAccessible, getCellOwnerId);

    return createNewCellMap().putArray(cells);
}

export function constructPlayerMap(
    gameSize: Coordinates,
    numberOfPlayers: number,
    getPlayerCoordinates: (playerId: number) => Coordinates,
): PlayerMap {
    const players = [];

    for (let playerId  = 0; playerId < numberOfPlayers; playerId++) {
        players.push(Player.create(
            playerId,
            getPlayerCoordinates(playerId),
        ));
    }

    return createNewPlayerMap().putArray(players);
}

export function constructDiagonalGame(gameDimensionalSize: number): Game {
    // players are set on the diagonal

    if (gameDimensionalSize < 2) {
        throw new Error('gameDimensionalSize must be greater than 1');
    }

    if (!Number.isInteger(gameDimensionalSize)) {
        throw new Error('gameDimensionalSize must be an integer');
    }

    const numberOfPlayers = gameDimensionalSize;
    const gameSize: Coordinates = [gameDimensionalSize, gameDimensionalSize];
    const coordinatesList = constructCoordinatesList(gameSize);

    const cellMap = constructCellMap(
        gameSize,
        coordinatesList,
        () => true,
        ([x, y]) => x === y ? x : undefined,
    );

    const playerMap = constructPlayerMap(
        gameSize,
        numberOfPlayers,
        (playerId) => [playerId, playerId],
    ) ; 

    return Game.initiate(gameSize, cellMap, playerMap);
}