import { IncomingCommands } from '../../src/commands/incomingCommands';
import { CommandType } from '../../src/commands/commandTypes';
import { Cell } from '../../src/types/cells';
import { OutgoingCommands } from '../../src/commands/outgoingCommands';
import { Direction } from '../../src/types/coordinates';

const ic01: IncomingCommands = [
    {
        commandType: CommandType.CREATE_GAME,
        playerId: 1,
        playerCoordinates: [0, 0],
        size: [2, 2],
        cells: [
            new Cell(0, [0, 0], true, 1),
            new Cell(1, [0, 1], true, undefined), // x * sizeY + y
            new Cell(2, [1, 0], true, undefined),
            new Cell(3, [1, 1], true, undefined),
        ]
    },
];

const oc01: OutgoingCommands = [
    {
        commandType: CommandType.GAME_INITIATED
    },
    {
        commandType: CommandType.CELL_OWNER_CHANGED,
        id: 0,
        newOwnerId: 1
    },
    {
        commandType: CommandType.CELL_OWNER_CHANGED,
        id: 1,
        newOwnerId: undefined
    },
    {
        commandType: CommandType.CELL_OWNER_CHANGED,
        id: 2,
        newOwnerId: undefined
    },
    {
        commandType: CommandType.CELL_OWNER_CHANGED,
        id: 3,
        newOwnerId: undefined
    },
    {
        commandType: CommandType.PLAYER_CREATED,
        id: 1
    },
    {
        commandType: CommandType.PLAYER_MOVED,
        id: 1,
        newCoordinates: [0, 0]
    },
];

const ic02: IncomingCommands = [
    {
        commandType: CommandType.JOIN_GAME,
        playerId: 2,
        playerCoordinates: [1, 1],
    }
];

const oc02: OutgoingCommands = [
    {
        id: 3,
        commandType: CommandType.CELL_OWNER_CHANGED,
        newOwnerId: 2
    },
    {
        commandType: CommandType.PLAYER_CREATED,
        id: 2
    },
    {
        commandType: CommandType.PLAYER_MOVED,
        id: 2,
        newCoordinates: [1, 1]
    }
];

const ic03: IncomingCommands = [
    {
        commandType: CommandType.ACKNOWLEDGE_GAME,
        playerId: 1,
    },
    {
        commandType: CommandType.ACKNOWLEDGE_GAME,
        playerId: 2,
    }
];

const oc03: OutgoingCommands = [
    { commandType: CommandType.PLAYER_ACKNOWLEDGED, id: 1 },
    { commandType: CommandType.PLAYER_ACKNOWLEDGED, id: 2 }
];

const ic04: IncomingCommands = [
    {
        commandType: CommandType.START_GAME,
    }
];

const oc04: OutgoingCommands = [
    { commandType: CommandType.GAME_STARTED },
    { commandType: CommandType.PLAYER_PLAYING, id: 1 },
    { commandType: CommandType.PLAYER_PLAYING, id: 2 }
];

const ic05: IncomingCommands = [
    {
        commandType: CommandType.MOVE_PLAYER,
        playerId: 1,
        direction: Direction.SOUTH,
    },
];

const oc05: OutgoingCommands = [
    {
        commandType: CommandType.CELL_OWNER_CHANGED,
        id: 1,
        newOwnerId: 1
    },
    {
        commandType: CommandType.PLAYER_MOVED,
        id: 1,
        newCoordinates: [0, 1]
    }
];

const ic06: IncomingCommands = [
    {
        commandType: CommandType.MOVE_PLAYER,
        playerId: 1,
        direction: Direction.EAST,
    },
];

const oc06: OutgoingCommands = [
    { commandType: CommandType.GAME_FINISHED },
    { 
        commandType: CommandType.CELL_OWNER_CHANGED,
        id: 3,
        newOwnerId: 1
    },
    { commandType: CommandType.PLAYER_WON, id: 1 },
    { commandType: CommandType.PLAYER_MOVED, id: 1, newCoordinates: [ 1, 1 ] },
    { commandType: CommandType.PLAYER_LOST, id: 2 }
];

export const protocol: ReadonlyArray<[IncomingCommands, OutgoingCommands]> = [
    [ic01, oc01],
    [ic02, oc02],
    [ic03, oc03],
    [ic04, oc04],
    [ic05, oc05],
    [ic06, oc06],
];