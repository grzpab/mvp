import 'mocha';
import { expect } from 'chai';

import { Engine, ExecuteIncomingCommands } from '../../src/engine';
import { OutgoingCommands } from '../../src/commands/outgoingCommands';
import { protocol } from '../integration/protocol01';

describe('Integration Tests', () => {
    it('integration test 01', (done) => {
        let _executeIncommingCommands: ExecuteIncomingCommands = () => { console.log('here'); };
        let _outgoingCommands: OutgoingCommands = [];

        const incomingCommandCallback = (executeIncommingCommands: ExecuteIncomingCommands) => {
            _executeIncommingCommands = executeIncommingCommands; 
        };

        const outgoingCommandCallback = (outgoingCommands: OutgoingCommands) => {
            _outgoingCommands = outgoingCommands;
        };
        
        new Engine(
            incomingCommandCallback,
            outgoingCommandCallback,
        );

        if (typeof _executeIncommingCommands === 'function') {
            for (const [ic, oc] of protocol) {
                _executeIncommingCommands(ic);
                expect(_outgoingCommands).to.be.deep.equal(oc);
            }
            done();
        }
    });
});