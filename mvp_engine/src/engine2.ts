import { Game } from './types/game';
import { IncomingCommands, CommandType, IncomingCommand } from '.';
import { executeCreateGameCommand } from './executables/executeCreateGameCommand';
import { executeJoinGameCommand } from './executables/executeJoinGameCommand';
import { executeAcknowledgeGameCommand } from './executables/executeAcknowledgeGameCommand';
import { executeStartGameCommand } from './executables/executeStartGameCommand';
import { executeYieldGameCommand } from './executables/executeYieldGameCommand';
import { executeMovePlayerCommand } from './executables/executeMovePlayerCommand';
import { getGameDelta } from './deltas/gameDeltas';
import { getOutgoingCommands } from './outgoingCommands';
import { OutgoingCommands } from './commands/outgoingCommands';

export class Engine2 {
    protected _game: Readonly<Game> | undefined;

    public constructor() {
        this._game = undefined;
    }

    public executeIncomingCommands(incomingCommands: IncomingCommands): OutgoingCommands {
        const oldGame = this._game;

        for (const incomingCommand of incomingCommands) {
            try {
                this._game = this.executeIncomingCommand(incomingCommand);
            } catch (error) {
                console.error(error);
            }
        }

        const newGame = this._game;
        const gameDelta = getGameDelta(oldGame, newGame);

        if (gameDelta === undefined) {
            return [];
        }

        return getOutgoingCommands(gameDelta);
    }

    protected executeIncomingCommand(incomingCommand: IncomingCommand): Readonly<Game> | undefined {
        switch (incomingCommand.commandType) {
            case CommandType.CREATE_GAME:
                return executeCreateGameCommand(incomingCommand);
            case CommandType.JOIN_GAME:
                return executeJoinGameCommand(incomingCommand, this._game);
            case CommandType.ACKNOWLEDGE_GAME:
                return executeAcknowledgeGameCommand(incomingCommand, this._game);
            case CommandType.START_GAME:
                return executeStartGameCommand(incomingCommand, this._game);
            case CommandType.YIELD_GAME:
                return executeYieldGameCommand(incomingCommand, this._game);
            case CommandType.MOVE_PLAYER:
                return executeMovePlayerCommand(incomingCommand, this._game);
            default:
                return this._game;
        }
    }
}