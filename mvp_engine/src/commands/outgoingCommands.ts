import { Coordinates } from '../types/coordinates';

export type GameInitiatedCommand = Readonly<{
    commandType: 'GAME_INITIATED';
}>;

export type GameStartedCommand = Readonly<{
    commandType: 'GAME_STARTED';
}>;

export type GameFinishedCommand = Readonly<{
    commandType: 'GAME_FINISHED';
}>;

export type CellOwnerChangedCommand = Readonly<{
    id: number,
    commandType: 'CELL_OWNER_CHANGED';
    newOwnerId: number | undefined;
}>;

export type PlayerCreatedCommand = Readonly<{
    commandType: 'PLAYER_CREATED';
    id: number;
}>;

export type PlayerAcknowledgedCommand = Readonly<{
    commandType: 'PLAYER_ACKNOWLEDGED';
    id: number;
}>;

export type PlayerPlayingCommand = Readonly<{
    commandType: 'PLAYER_PLAYING';
    id: number;
}>;

export type PlayerLostCommand = Readonly<{
    commandType: 'PLAYER_LOST';
    id: number;
}>;

export type PlayerYieldedCommand = Readonly<{
    commandType: 'PLAYER_YIELDED';
    id: number;
}>;

export type PlayerWonCommand = Readonly<{
    commandType: 'PLAYER_WON';
    id: number;
}>;

export type PlayerMovedCommand = Readonly<{
    commandType: 'PLAYER_MOVED';
    id: number;
    newCoordinates: Coordinates | undefined;
}>;

export type OutgoingGameCommand = 
    GameInitiatedCommand |
    GameStartedCommand |
    GameFinishedCommand;

export type OutgoingCellCommand =
    CellOwnerChangedCommand;

export type PlayerStatusCommand =
    PlayerCreatedCommand |
    PlayerAcknowledgedCommand |
    PlayerPlayingCommand |
    PlayerLostCommand |
    PlayerYieldedCommand |
    PlayerWonCommand;

export type OutgoingPlayerCommand =
    PlayerStatusCommand |
    PlayerMovedCommand;

export type OutgoingCommand =
    OutgoingGameCommand |
    OutgoingCellCommand |
    OutgoingPlayerCommand;

export type OutgoingCommands = ReadonlyArray<OutgoingCommand>;

export type EventType = OutgoingCommand['commandType'];