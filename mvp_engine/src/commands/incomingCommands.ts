import { Coordinates, Direction } from '../types/coordinates';
import { CellArray } from '../types/cells';

export type CreateGameCommand = Readonly<{
    commandType: 'CREATE_GAME';
    playerId: number;
    playerCoordinates: Coordinates;
    size: Coordinates;
    cells: CellArray;
}>;

export type JoinGameCommand = Readonly<{
    commandType: 'JOIN_GAME';
    playerId: number;
    playerCoordinates: Coordinates;
}>;

export type AcknowledgeGameCommand = Readonly<{
    commandType: 'ACKNOWLEDGE_GAME';
    playerId: number;
}>;

export type StartGameCommand = Readonly<{
    commandType: 'START_GAME';
}>;

export type MovePlayerCommand = Readonly<{
    commandType: 'MOVE_PLAYER';
    direction: Direction;
    playerId: number;
}>;

export type YieldGameCommand = Readonly<{
    commandType: 'YIELD_GAME';
    playerId: number;
}>;

export type IncomingCommand =
    CreateGameCommand |
    JoinGameCommand |
    MovePlayerCommand |
    AcknowledgeGameCommand |
    YieldGameCommand |
    StartGameCommand;

export type IncomingCommands = ReadonlyArray<IncomingCommand>;

export type CommandType = IncomingCommand['commandType'];