import { Game, GameStatus } from '../types/game';
import { PlayerStatus, Player } from '../types/players';
import {
    IncorrectPlayerStatusError,
    PlayerHasNoCoordinatesError,
    GameDoesNotExistError,
    IncorrectGameStatusError,
    CellNotFoundError,
    CellNotAccessibleError,
    PlayerNotFoundError
} from '../errors';
import { calculateNewCoordinates } from '../types/coordinates';
import * as _ from 'lodash';
import { Cell } from '../types/cells';
import { MovePlayerCommand } from '../commands/incomingCommands';
import { getCellId } from '../utilities/cellUtilities';

export function executeMovePlayerCommand(
    command: MovePlayerCommand,
    oldGame: Readonly<Game> | undefined
): Game {
    if (oldGame === undefined) {
        throw new GameDoesNotExistError();
    }

    if (oldGame.status !== GameStatus.STARTED) {
        throw new IncorrectGameStatusError();
    }

    const { playerId, direction } = command;

    const oldPlayer = oldGame.getPlayer(playerId);

    if (oldPlayer === undefined) {
        throw new PlayerNotFoundError();
    }

    if (oldPlayer.status !== PlayerStatus.PLAYING) {
        throw new IncorrectPlayerStatusError();
    }

    if (oldPlayer.coordinates === undefined) {
        throw new PlayerHasNoCoordinatesError();
    }

    const coordinates = calculateNewCoordinates(oldPlayer.coordinates, direction);

    let cellId: number | undefined = undefined;

    try {
        cellId = getCellId(oldGame.size, coordinates);
    } catch (e) {
        cellId = undefined;
    }

    const cell = cellId ? oldGame.getCell(cellId) : undefined;

    if (cell === undefined) {
        throw new CellNotFoundError();
    }

    if (cell.accessible === false) {
        throw new CellNotAccessibleError();
    }

    const otherPlayer = oldGame
        .filterPlayers(p => p.status === PlayerStatus.PLAYING && p.id !== playerId)
        .find(player => _.isEqual(player.coordinates, coordinates));

    if (otherPlayer === undefined) {
        return movePlayerIntoOwnerlessCell(oldPlayer, cell, oldGame);
    }

    const numberOfOldPlayerCells = oldGame
        .filterCells(c => c.ownerId === playerId)
        .length;
            
    const numberOfOtherPlayerCells = oldGame
        .filterCells(c => c.ownerId === otherPlayer.id)
        .length;

    if (numberOfOtherPlayerCells > numberOfOldPlayerCells) {
        return destroyOldPlayer(oldPlayer, oldGame).tryFinishing();
    }

    if (numberOfOtherPlayerCells === numberOfOldPlayerCells) {
        return destroyBothPlayers(
            oldPlayer,
            otherPlayer,
            cell,
            oldGame,
        ).tryFinishing();
    }

    return destroyOtherPlayerAndMoveOldPlayer(
        oldPlayer,
        otherPlayer,
        cell,
        oldGame,
    ).tryFinishing();
}

export function destroyOldPlayer(
    oldPlayer: Player,
    oldGame: Readonly<Game>,
): Game {
    const newPlayer = oldPlayer.makeLose();

    return oldGame.mergeEntities([], [newPlayer]);
}

export function destroyBothPlayers(
    oldPlayer: Player,
    otherPlayer: Player,
    cell: Cell,
    oldGame: Readonly<Game>,
): Game {
    const newPlayer = oldPlayer.makeLose();
    const newOtherPlayer = otherPlayer.makeLose();
    
    const newCell = cell.setOwnerId(undefined);

    return oldGame.mergeEntities([newCell], [newPlayer, newOtherPlayer]);
}

export function destroyOtherPlayerAndMoveOldPlayer(
    oldPlayer: Player,
    otherPlayer: Player,
    cell: Cell,
    oldGame: Readonly<Game>,
): Game {
    const newOtherPlayer = otherPlayer.makeLose();
    const newPlayer = oldPlayer.setCoordinates(cell.coordinates);

    const newCell = cell.setOwnerId(newPlayer.id);

    return oldGame.mergeEntities([newCell], [newPlayer, newOtherPlayer]);
}

export function movePlayerIntoOwnerlessCell(
    oldPlayer: Player,
    cell: Cell,
    oldGame: Readonly<Game>,
): Game {
    const newCell = cell.setOwnerId(oldPlayer.id);

    const newPlayer = oldPlayer.setCoordinates(cell.coordinates);

    return oldGame.mergeEntities([newCell], [newPlayer]);
}