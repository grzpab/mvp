import * as _ from 'lodash';

import { StartGameCommand } from '../commands/incomingCommands';
import { Game, GameStatus } from '../types/game';
import { IncorrectGameStatusError, GameDoesNotExistError } from '../errors';
import { PlayerStatus, Player } from '../types/players';
import { Cell } from '../types/cells';

export function executeStartGameCommand(
    command: StartGameCommand,
    oldGame: Readonly<Game> | undefined
): Game {
    if (oldGame === undefined) {
        throw new GameDoesNotExistError();
    }
    
    if (oldGame.status !== GameStatus.INITIATED) {
        throw new IncorrectGameStatusError();
    }
    
    const nonAcknowledgedPlayers = oldGame
        .filterPlayers(player => player.status !== PlayerStatus.ACKNOWLEDGED);

    const acknowledgedPlayers = oldGame
        .filterPlayers(player => player.status === PlayerStatus.ACKNOWLEDGED);

    const newCells: ReadonlyArray<Cell> = _
        .chain(nonAcknowledgedPlayers)
        .map(player => player.coordinates)
        .compact()
        .map(coordinates => oldGame.getCellByCoordinates(coordinates))
        .compact()
        .map((cell: Cell) => cell.setOwnerId(undefined))
        .value();

    const lostPlayers = nonAcknowledgedPlayers.map(player => player.makeLose());
    const playingPlayers = acknowledgedPlayers.map(player => player.makePlaying());

    const newPlayers = ([] as Array<Player>).concat(
        lostPlayers,
        playingPlayers,
    );

    const nonStartedGame = oldGame.mergeEntities(newCells, newPlayers);
    
    if (nonStartedGame.getPlayingPlayers().length === 0) {
        return nonStartedGame.tryFinishing();
    }

    return nonStartedGame.start();
}