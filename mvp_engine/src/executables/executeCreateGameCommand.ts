import { Game } from '../types/game';
import { createNewPlayerMap } from '../utilities/playerUtilities';
import { CreateGameCommand } from '../commands/incomingCommands';
import { createNewCellMap, areCoordinatesInBounds } from '../utilities/cellUtilities';
import { Player } from '../types/players';
import { CoordinatesOutOfBoundsError } from '../errors';

export function executeCreateGameCommand(command: CreateGameCommand): Game {
    const { size, playerId, playerCoordinates, cells} = command;

    if (areCoordinatesInBounds(size, playerCoordinates) === false) {
        throw new CoordinatesOutOfBoundsError();
    }

    const player = Player.create(playerId, playerCoordinates);
    const playerMap = createNewPlayerMap().put(player);
    const cellMap = createNewCellMap().putArray(cells);
    
    return Game.initiate(
        size,
        cellMap,
        playerMap,
    );
}