import { Game, GameStatus } from '../types/game';
import {
    PlayerNotFoundError,
    IncorrectPlayerStatusError,
    PlayerHasNoCoordinatesError,
    CellNotFoundError,
    GameDoesNotExistError,
} from '../errors';
import { PlayerStatus } from '../types/players';
import { YieldGameCommand } from '../commands/incomingCommands';

export function executeYieldGameCommand(
    command: YieldGameCommand,
    oldGame: Readonly<Game> | undefined)
: Game {
    if (oldGame === undefined) {
        throw new GameDoesNotExistError();
    }

    if (oldGame.status !== GameStatus.STARTED) {
        throw new GameDoesNotExistError();
    }

    const { playerId } = command;

    const oldPlayer = oldGame.getPlayer(playerId);

    if (oldPlayer === undefined) {
        throw new PlayerNotFoundError();
    }

    const { status, coordinates } = oldPlayer;

    if (status !== PlayerStatus.PLAYING) {
        throw new IncorrectPlayerStatusError();
    }

    if (coordinates === undefined) {
        throw new PlayerHasNoCoordinatesError();
    }

    const oldCell = oldGame.getCellByCoordinates(coordinates);

    if (oldCell === undefined) {
        throw new CellNotFoundError();
    }

    const newPlayer = oldPlayer.makeYield();
    const newCell = oldCell.setOwnerId(undefined);

    return oldGame.mergeEntities([newCell], [newPlayer]).tryFinishing();
}