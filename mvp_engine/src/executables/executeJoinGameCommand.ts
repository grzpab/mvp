import { Game, GameStatus } from '../types/game';
import {
    CellNotFoundError,
    PlayerAlreadyExistsError,
    IncorrectGameStatusError,
    GameDoesNotExistError,
    CellAlreadyOwnerError,
} from '../errors';
import { JoinGameCommand } from '../commands/incomingCommands';
import { Player } from '../types/players';

export function executeJoinGameCommand(
    command: JoinGameCommand,
    oldGame: Readonly<Game> | undefined
): Game {
    if (oldGame === undefined) {
        throw new GameDoesNotExistError();
    }
    
    if (oldGame.status !== GameStatus.INITIATED) {
        throw new IncorrectGameStatusError();
    }

    const { playerId, playerCoordinates} = command;

    const oldPlayer = oldGame.getPlayer(playerId);

    if (oldPlayer !== undefined) {
        throw new PlayerAlreadyExistsError();
    }

    const cell = oldGame.getCellByCoordinates(playerCoordinates);

    if (cell === undefined) {
        throw new CellNotFoundError();
    }

    if (cell.ownerId !== undefined) {
        throw new CellAlreadyOwnerError();
    }

    const newPlayer = Player.create(playerId, playerCoordinates);
    const newCell = cell.setOwnerId(playerId);

    return oldGame.mergeEntities([newCell], [newPlayer]);
}