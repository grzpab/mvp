import { Game, GameStatus } from '../types/game';
import { PlayerStatus } from '../types/players';
import {
    PlayerNotFoundError,
    IncorrectPlayerStatusError,
    IncorrectGameStatusError,
    GameDoesNotExistError
} from '../errors';
import { AcknowledgeGameCommand } from '../commands/incomingCommands';

export function executeAcknowledgeGameCommand(
    command: AcknowledgeGameCommand,
    oldGame: Readonly<Game> | undefined
): Game {
    if (oldGame === undefined) {
        throw new GameDoesNotExistError('GameDoesNotExistError');
    }
    
    if (oldGame.status !== GameStatus.INITIATED) {
        throw new IncorrectGameStatusError();
    }
    
    const { playerId } = command;

    const oldPlayer = oldGame.getPlayer(playerId);

    if (oldPlayer === undefined) {
        throw new PlayerNotFoundError();
    }

    if (oldPlayer.status !== PlayerStatus.CREATED) {
        throw new IncorrectPlayerStatusError();
    }

    const player = oldPlayer.acknowledge();

    return oldGame.mergeEntities([], [player]);
}