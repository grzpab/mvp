export const SERVER_HOST: string = '127.0.0.1';
export const SERVER_PORT: number = 12121;
export const WEB_SOCKET_SERVER_PORT: number = 8080;

export const API_VERSION: string = '0.0.1';

export const PACKET_PREAMBLE: number = 0xA1B2C3D4;
export const PACKET_POSTAMBLE: number = 0xD4C3B2A1;
export const PACKET_XOR_ARGUMENT: number = 0x10;