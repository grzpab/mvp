import { IncomingCommands } from '../commands/incomingCommands';
import { OutgoingCommands } from '../commands/outgoingCommands';

export type ClientHelloMessage = Readonly<{
    type: 'CLIENT_HELLO';
    clientId: number | undefined;
}>;

export type ServerHelloMessage = Readonly<{
    type: 'SERVER_HELLO';
    clientId: number;
}>;

export type ClientCommandMessage = Readonly<{
    type: 'CLIENT_COMMAND';
    clientId: number;
    gameId: number | undefined;
    commands: IncomingCommands;
}>;

export type ServerCommandMessage = Readonly<{
    type: 'SERVER_COMMAND';
    clientId: number;
    gameId: number;
    commands: OutgoingCommands;
}>;

export type ClientPingMessage = Readonly<{
    type: 'CLIENT_PING';
    clientId: number;
    gameId: number;
}>;

export type ServerPongMessage = Readonly<{
    type: 'SERVER_PONG';
    clientId: number;
    gameId: number;
    commands: OutgoingCommands;
}>;

export type ClientMessage =
    ClientHelloMessage |
    ClientCommandMessage |
    ClientPingMessage;

export type ClientMessageType = ClientMessage['type'];

export type ServerMessage = 
    ServerHelloMessage |
    ServerCommandMessage |
    ServerPongMessage;

export type ServerMessageType = ServerMessage['type'];

export type ServerMessages = ReadonlyArray<ServerMessage>;

export type Message = ClientMessage | ServerMessage;

export type Messages = ReadonlyArray<Message>;