import {
    PACKET_PREAMBLE,
    PACKET_POSTAMBLE,
    PACKET_XOR_ARGUMENT,
} from './constants';

import {
    Message
} from './messageTypes';

export function getMessage(data: Buffer): Message | undefined {
    const newData = data.map(i => i ^ PACKET_XOR_ARGUMENT) as Buffer;
    const preamble = newData.readUInt32BE(0);

    if (preamble !== PACKET_PREAMBLE) {
        return undefined;
    }

    const byteLength = newData.readUInt32BE(4);

    const postamble = newData.readUInt32BE(8 + byteLength);

    if (postamble !== PACKET_POSTAMBLE) {
        return undefined;
    }

    const bytes = newData.toString('utf-8', 8, 8 + byteLength);
    const message = JSON.parse(bytes);

    return message;
}

export function createPacket(message: Message): Uint8Array {
    const stringifiedMessage = JSON.stringify(message);
    const byteLength = Buffer.byteLength(stringifiedMessage);
    const buffer = new Buffer(8 + byteLength + 4);

    buffer.writeUInt32BE(PACKET_PREAMBLE, 0);
    buffer.writeUInt32BE(byteLength, 4);
    buffer.write(stringifiedMessage, 8);
    buffer.writeUInt32BE(PACKET_POSTAMBLE, 8 + byteLength);

    const transformedBuffer = buffer.map(i => i ^ PACKET_XOR_ARGUMENT);

    return transformedBuffer;
}