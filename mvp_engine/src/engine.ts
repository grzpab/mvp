import { Game } from './types/game';
import { IncomingCommands, IncomingCommand } from './commands/incomingCommands';
import { OutgoingCommands } from './commands/outgoingCommands';
import { CommandType } from './commands/commandTypes';
import { executeCreateGameCommand } from './executables/executeCreateGameCommand';
import { executeJoinGameCommand } from './executables/executeJoinGameCommand';
import { executeAcknowledgeGameCommand } from './executables/executeAcknowledgeGameCommand';
import { executeStartGameCommand } from './executables/executeStartGameCommand';
import { executeYieldGameCommand } from './executables/executeYieldGameCommand';
import { executeMovePlayerCommand } from './executables/executeMovePlayerCommand';
import { getGameDelta } from './deltas/gameDeltas';
import { getOutgoingCommands } from './outgoingCommands';

export type ExecuteIncomingCommands = (incomingCommands: IncomingCommands) => void;
export type IncomingCommandCallback = (executeIncomingCommands: ExecuteIncomingCommands) => void;
export type OutgoingCommandCallback = (outgoingCommands: OutgoingCommands) => void;

export class Engine {
    protected incomingCommandCallback: IncomingCommandCallback;
    protected outgoingCommandCallback: OutgoingCommandCallback;
    protected _game: Readonly<Game> | undefined;

    public constructor(
        incomingCommandCallback: IncomingCommandCallback,
        outgoingCommandCallback: OutgoingCommandCallback,
    ) {
        this.incomingCommandCallback = incomingCommandCallback;
        this.outgoingCommandCallback = outgoingCommandCallback;

        this.incomingCommandCallback(this.executeIncomingCommands.bind(this));
    }

    public executeIncomingCommands(incomingCommands: IncomingCommands): void {
        // TODO sort commands

        const oldGame = this._game;

        for (const incomingCommand of incomingCommands) {
            try {
                this._game = this.executeIncomingCommand(incomingCommand);
            } catch (error) {
                console.error(error);
            }
        }

        const newGame = this._game;

        const gameDelta = getGameDelta(oldGame, newGame);

        if (gameDelta === undefined) {
            this.outgoingCommandCallback([]);
        } else {
            const outgoingCommands = getOutgoingCommands(gameDelta);
            this.outgoingCommandCallback(outgoingCommands);
        }
    }

    protected executeIncomingCommand(incomingCommand: IncomingCommand): Readonly<Game> | undefined {
        switch (incomingCommand.commandType) {
            case CommandType.CREATE_GAME:
                return executeCreateGameCommand(incomingCommand);
            case CommandType.JOIN_GAME:
                return executeJoinGameCommand(incomingCommand, this._game);
            case CommandType.ACKNOWLEDGE_GAME:
                return executeAcknowledgeGameCommand(incomingCommand, this._game);
            case CommandType.START_GAME:
                return executeStartGameCommand(incomingCommand, this._game);
            case CommandType.YIELD_GAME:
                return executeYieldGameCommand(incomingCommand, this._game);
            case CommandType.MOVE_PLAYER:
                return executeMovePlayerCommand(incomingCommand, this._game);
            default:
                return this._game;
        }
    }

    public get game(): Readonly<Game> | undefined {
        return this._game;
    }
}