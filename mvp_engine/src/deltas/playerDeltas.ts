import * as _ from 'lodash';

import { Player, PlayerDelta, PlayerStatus } from '../types/players';
import { PlayerIdMismatchError, PlayerDeltaRuntimeError } from '../errors';

export const getPlayerDelta =
    (oldPlayer: Player | undefined) =>
    (newPlayer: Player | undefined): PlayerDelta | undefined =>  {
    if (_.isEqual(
        _.pick(oldPlayer, ['id', 'status', 'coordinates']),
        _.pick(newPlayer, ['id', 'status', 'coordinates'])
    )) {
        return undefined;
    }

    if (oldPlayer === undefined && newPlayer !== undefined) {
        return {
            id: newPlayer.id,
            newStatus: newPlayer.status,
            newCoordinates: newPlayer.coordinates,
            statusChanged : true,
            coordinatesChanged: true,
        };
    }

    if (oldPlayer !== undefined && newPlayer !== undefined) {
        const {
            id: oldId,
            status: oldStatus,
            coordinates: oldCoordinates,
        } = oldPlayer;
        
        const {
            id: newId,
            status: newStatus,
            coordinates: newCoordinates,
        } = newPlayer;

        if (oldId !== newId) {
            throw new PlayerIdMismatchError();
        }

        const statusChanged = oldStatus !== newStatus;
        const coordinatesChanged = !_.isEqual(
            oldCoordinates,
            newCoordinates
        );
    
        return {
            id: newId,
            newStatus,
            newCoordinates,
            statusChanged,
            coordinatesChanged,
        };
    }

    throw new PlayerDeltaRuntimeError();
};