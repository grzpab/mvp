import * as _ from 'lodash';
import { Game, GameDelta } from '../types/game';
import { getCellDelta } from './cellDeltas';
import { getPlayerDelta } from './playerDeltas';
import { GameDeltaRuntimeError } from '../errors';

export function getGameDelta(
    oldGame: Readonly<Game> | undefined,
    newGame: Readonly<Game> | undefined
): GameDelta | undefined {
    if (oldGame === newGame) {
        return undefined;
    }

    if (oldGame === undefined && newGame !== undefined) {
        const getCellDeltaForwarded = getCellDelta(undefined);

        const cellDeltas = _
            .chain(newGame.getCells())
            .map(getCellDeltaForwarded)
            .compact()
            .value();

        const getPlayerDeltaForwarded = getPlayerDelta(undefined);

        const playerDeltas = _ 
            .chain(newGame.getPlayers())
            .map(getPlayerDeltaForwarded)
            .compact()
            .value();

        return {
            newStatus: newGame.status,
            statusChanged: true,
            cellDeltas,
            playerDeltas,
        };
    }

    if (oldGame !== undefined && newGame !== undefined) {
        const { status: newStatus } = newGame;
        const { status: oldStatus } = oldGame;

        const statusChanged = newStatus !== oldStatus;

        const cellDeltas = _
            .chain(oldGame.getCells())
            .zip(newGame.getCells())
            .map(([oldCell, newCell]) => getCellDelta(oldCell)(newCell))
            .compact()
            .value();

        const playerDeltas = _
            .chain(oldGame.getPlayers())
            .zip(newGame.getPlayers())
            .map(([oldPlayer, newPlayer]) => getPlayerDelta(oldPlayer)(newPlayer))
            .compact()
            .value();

        return {
            newStatus,
            statusChanged,
            cellDeltas,
            playerDeltas,
        };
    }

    throw new GameDeltaRuntimeError();
}