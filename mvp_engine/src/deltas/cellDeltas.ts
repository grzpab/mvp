import * as _ from 'lodash';
import { Cell, CellDelta } from '../types/cells';
import { CellIdMismatchError, CellDeltaRuntimeError } from '../errors';

export const getCellDelta =
    (oldCell: Cell | undefined) =>
    (newCell: Cell | undefined): CellDelta | undefined => {
    if (_.isEqual(oldCell, newCell)) {
        return undefined;
    }

    if (oldCell === undefined && newCell !== undefined) {
        return {
            id: newCell.id,
            newOwnerId: newCell.ownerId,
            ownerIdChanged: true,
        };
    }

    if (oldCell !== undefined && newCell !== undefined) {
        const { 
            id: oldId,
            ownerId : oldOwnerId
        } = oldCell;

        const { 
            id: newId,
            ownerId : newOwnerId
        } = newCell;

        if (oldId !== newId) {
            throw new CellIdMismatchError();
        }

        const ownerIdChanged = !_.isEqual(oldOwnerId, newOwnerId);
    
        return {
            id: newId,
            newOwnerId,
            ownerIdChanged,
        };
    }

    throw new CellDeltaRuntimeError();
};