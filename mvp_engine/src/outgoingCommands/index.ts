import { GameDelta } from '../types/game';
import { OutgoingCommands, OutgoingCommand } from '../commands/outgoingCommands';
import * as _ from 'lodash';
import { getOutgoingCommandOnGameStatus } from './gameStatus';
import { getOutgoingCommandOnCellDelta } from './cellDelta';
import { getOutgoingCommandsOnPlayerDelta } from './playerDelta';

export function getOutgoingCommands(gameDelta: GameDelta): OutgoingCommands {
    const {
        statusChanged,
        newStatus,
        cellDeltas,
        playerDeltas,
    } = gameDelta;

    const gameStatusCommands: OutgoingCommands = _.compact([
        statusChanged ? getOutgoingCommandOnGameStatus(newStatus) : undefined
    ]);
    
    const cellCommands: OutgoingCommands = _
        .chain(cellDeltas)
        .map(getOutgoingCommandOnCellDelta)
        .compact()
        .value();

    const playerCommands: OutgoingCommands = _
        .chain(playerDeltas)
        .map(getOutgoingCommandsOnPlayerDelta)
        .reduce(
            (a, b) => a.concat(b),
            [] as Array<OutgoingCommand>,
        )
        .value();

    return ([] as Array<OutgoingCommand>).concat(
        gameStatusCommands,
        cellCommands,
        playerCommands,
    );
}