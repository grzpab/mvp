import { PlayerDelta, PlayerStatus } from '../types/players';
import {
    PlayerMovedCommand,
    PlayerStatusCommand,
    OutgoingPlayerCommand
} from '../commands/outgoingCommands';
import { Coordinates } from '../types/coordinates';
import * as _ from 'lodash';

export const getOutgoingCommandOnPlayerCoordinates = (
    id: number,
    newCoordinates: Coordinates | undefined
): PlayerMovedCommand => ({
    commandType: 'PLAYER_MOVED',
    id,
    newCoordinates,
});

export const getOutgoingCommandOnPlayerStatus = (
        id: number,
        newPlayerStatus: PlayerStatus
    ): PlayerStatusCommand | undefined => {
    switch (newPlayerStatus) {
        case PlayerStatus.CREATED:
            return {
                commandType: 'PLAYER_CREATED',
                id,
            };
        case PlayerStatus.ACKNOWLEDGED:
            return {
                commandType: 'PLAYER_ACKNOWLEDGED',
                id,
            };
        case PlayerStatus.PLAYING:
            return {
                commandType: 'PLAYER_PLAYING',
                id,
            };
        case PlayerStatus.LOST:
            return {
                commandType: 'PLAYER_LOST',
                id,
            };
        case PlayerStatus.YIELDED:
            return {
                commandType: 'PLAYER_YIELDED',
                id,
            };
        case PlayerStatus.WON:
            return {
                commandType: 'PLAYER_WON',
                id,
            };
        default:
            return undefined;
    }
};

export function getOutgoingCommandsOnPlayerDelta(playerDelta: PlayerDelta):
    ReadonlyArray<OutgoingPlayerCommand> {
    const {
        id,
        statusChanged,
        coordinatesChanged,
        newStatus,
        newCoordinates,
    } = playerDelta;

    const statusCommand = statusChanged ?
        getOutgoingCommandOnPlayerStatus(id, newStatus) :
        undefined;
    
    const coordinatesCommand = coordinatesChanged ?
        getOutgoingCommandOnPlayerCoordinates(id, newCoordinates) :
        undefined;

    return _.compact([statusCommand, coordinatesCommand]);
}
