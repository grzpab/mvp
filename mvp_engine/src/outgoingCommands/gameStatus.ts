import { OutgoingGameCommand } from '../commands/outgoingCommands';
import { GameStatus } from '../types/game';

export const getOutgoingCommandOnGameStatus = (newGameStatus: GameStatus):
    OutgoingGameCommand | undefined => {
    switch (newGameStatus) {
        case GameStatus.INITIATED:
            return {
                commandType: 'GAME_INITIATED',
            };
        case GameStatus.STARTED:
            return {
                commandType: 'GAME_STARTED',
            };
        case GameStatus.FINISHED:
            return {
                commandType: 'GAME_FINISHED',
            };
        default:
            return undefined;
    }
};