import { CellDelta } from '../types/cells';
import { OutgoingCommand } from '../commands/outgoingCommands';

export const getOutgoingCommandOnCellDelta = (cellDelta: CellDelta)
: OutgoingCommand | undefined => {
    const { id, ownerIdChanged, newOwnerId } = cellDelta;
    
    if (ownerIdChanged === false) {
        return undefined;
    }

    return {
        id,
        commandType: 'CELL_OWNER_CHANGED',
        newOwnerId,
    };
};