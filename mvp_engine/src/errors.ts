export class InheritableError extends Error {
    constructor(message?: string) {
        super(message);
        
        this.name = this.constructor.name;
    
        if (typeof Error.captureStackTrace === 'function') {
            Error.captureStackTrace(this, this.constructor);
        } else {
            this.stack = (new Error(message)).stack; 
        }
    }
}    

export class IncorrectGameStatusError extends InheritableError {}
export class GameDoesNotExistError extends InheritableError {}

export class GameDeltaRuntimeError extends InheritableError {}

export class PlayerIdMismatchError extends InheritableError {}
export class IncorrectPlayerStatusError extends InheritableError {}
export class PlayerAlreadyExistsError extends InheritableError {}
export class PlayerNotFoundError extends InheritableError {}
export class PlayerHasNoCoordinatesError extends InheritableError {}
export class NoPlayingPlayersError extends InheritableError {}
export class PlayerDeltaRuntimeError extends InheritableError {}

export class CellIdMismatchError extends InheritableError {}
export class CellNotFoundError extends InheritableError {}
export class CellOutOfBoundsError extends InheritableError {}
export class CellNotAccessibleError extends InheritableError {}
export class CellAlreadyOwnerError extends InheritableError {}
export class CellDeltaRuntimeError extends InheritableError {}

export class CoordinatesOutOfBoundsError extends InheritableError {}