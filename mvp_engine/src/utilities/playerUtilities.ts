import {
    Player,
    PlayerMap,
    PlayerArray,
} from '../types/players';
import { FMap } from '../types/map';

export const createNewPlayerMap = () => new FMap<number, Player>(player => player.id);

export function mergeNewPlayers (
    oldPlayerMap: PlayerMap,
    newPlayers: PlayerArray
): PlayerMap {
    if (newPlayers.length === 0) {
        return oldPlayerMap;
    }

    return new FMap<number, Player>(player => player.id)
        .putMap(oldPlayerMap)
        .putArray(newPlayers);
}