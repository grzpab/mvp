import { CellOutOfBoundsError } from '../errors';
import { FMap } from '../types/map';
import { Cell, CellMap, CellArray } from '../types/cells';
import { Coordinates } from '../types/coordinates';

export const createNewCellMap = () => new FMap<number, Cell>(cell => cell.id);

export const areCoordinatesInBounds = (bounds: Coordinates, coordinates: Coordinates) => {
    const [ boundX, boundY ] = bounds;
    const [x, y] = coordinates;

    return x >= 0 && x < boundX && y >= 0 && y < boundY;
};

export const getCellId = (gameSize: Coordinates, cellCoordinates: Coordinates) => {
    if (areCoordinatesInBounds(gameSize, cellCoordinates) === false) {
        throw new CellOutOfBoundsError();
    }

    const [ sizeY ] = gameSize;
    const [ x, y ] = cellCoordinates;

    return x * sizeY + y;
};

export function mergeNewCells (oldCellMap: CellMap, newCells: CellArray): CellMap {
    if (newCells.length === 0) {
        return oldCellMap;
    }

    return createNewCellMap()
        .putMap(oldCellMap)
        .putArray(newCells);
}