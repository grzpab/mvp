import { CellDelta, CellMap, Cell, CellArray } from './cells';
import { PlayerDelta, PlayerMap, Player, PlayerArray, PlayerStatus } from './players';
import { Coordinates } from './coordinates';
import { getCellId, mergeNewCells } from '../utilities/cellUtilities';
import { mergeNewPlayers, createNewPlayerMap } from '../utilities/playerUtilities';
import { FMap } from './map';

export const enum GameStatus {
    INITIATED = 'INITIATED',
    STARTED = 'STARTED',
    FINISHED = 'FINISHED',
}

export class Game {
    public readonly size: Coordinates;
    public readonly status: GameStatus;
    protected readonly cellMap: CellMap;
    protected readonly playerMap: PlayerMap;

    protected constructor(
        size: Coordinates,
        status: GameStatus,
        cellMap: CellMap,
        playerMap: PlayerMap
    ) {
        this.size = size;
        this.status = status;
        this.cellMap = cellMap;
        this.playerMap = playerMap;
    }

    public static initiate = (
        size: Coordinates,
        cellMap: CellMap,
        playerMap: PlayerMap
    ) => new Game(
        size,
        GameStatus.INITIATED,
        cellMap,
        playerMap,
    )

    public start = () => {
        return new Game(
            this.size,
            GameStatus.STARTED,
            this.cellMap,
            this.playerMap,
        );
    }

    public mergeEntities = (newCells: CellArray, newPlayers: PlayerArray): Game => {
        const newCellMap = mergeNewCells(this.cellMap, newCells);
        const newPlayerMap = mergeNewPlayers(this.playerMap, newPlayers);

        return new Game(
            this.size,
            this.status,
            newCellMap,
            newPlayerMap,
        );
    }

    public tryFinishing = (): Game => {
        const playingPlayers = this.getPlayingPlayers();

        if (playingPlayers.length > 1) {
            return this;
        }

        if (playingPlayers.length === 0) {
            return new Game(
                this.size,
                GameStatus.FINISHED,
                this.cellMap,
                this.playerMap,
            );
        }
    
        const winner = playingPlayers[0].makeWin();
    
        const { id: winnerId } = winner;
    
        const losers = this
                .filterPlayers(player => player.id !== winnerId)
                .map(player => player.makeLose());
    
        const newPlayerMap = createNewPlayerMap()
                .put(winner)
                .putArray(losers);
    
        return new Game(
            this.size,
            GameStatus.FINISHED,
            this.cellMap,
            newPlayerMap,
        );
    }

    public getPlayer(playerId: number): Player | undefined {
        return this.playerMap.get(playerId);
    }

    public getCell(cellId: number): Cell | undefined {
        return this.cellMap.get(cellId);
    }

    public getCellByCoordinates(coordinates: Coordinates): Cell | undefined {
        try {
            const cellId = getCellId(this.size, coordinates);
            return this.getCell(cellId);
        } catch (e) {
            return undefined;
        }
    }

    public filterPlayers(predicate: (player: Player) => boolean): PlayerArray {
        return this.playerMap.values().filter(predicate);
    }

    public getPlayingPlayers(): PlayerArray {
        return this.filterPlayers(player => player.status === PlayerStatus.PLAYING);
    }

    public filterCells(predicate: (cell: Cell) => boolean): CellArray {
        return this.getCells().filter(predicate);
    }

    public getCells(): CellArray {
        return this.cellMap.values();
    }

    public getPlayers(): PlayerArray {
        return this.playerMap.values();
    }
}

export type GameDelta = Readonly<{
    newStatus: GameStatus;
    statusChanged: boolean;
    cellDeltas: ReadonlyArray<CellDelta>;
    playerDeltas: ReadonlyArray<PlayerDelta>;
}>;