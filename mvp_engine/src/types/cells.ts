import { Coordinates } from './coordinates';
import { FMap } from './map';

export class Cell {
    public readonly id: number;
    public readonly coordinates: Coordinates;
    public readonly accessible: boolean;
    public readonly ownerId: number | undefined;

    public constructor(
        id: number,
        coordinates: Coordinates,
        accessible: boolean,
        ownerId: number | undefined
    ) {
        this.id = id;
        this.coordinates = coordinates;
        this.accessible = accessible;
        this.ownerId = ownerId;
    }

    public setOwnerId(ownerId: number | undefined): Cell {
        return new Cell(
            this.id,
            this.coordinates,
            this.accessible,
            ownerId,
        );
    }

    public setAccessible(accessible: boolean): Cell {
        return new Cell(
            this.id,
            this.coordinates,
            accessible,
            this.ownerId,
        );
    }
}

export type CellMap = FMap<number, Cell>;
export type CellArray = ReadonlyArray<Cell>;

export type CellDelta = Readonly<{
    id: number,
    newOwnerId: number | undefined;
    ownerIdChanged: boolean;
}>;
