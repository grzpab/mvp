import { Coordinates } from './coordinates';
import { FMap } from './map';

export const enum PlayerStatus {
    CREATED = 'CREATED',
    ACKNOWLEDGED = 'ACKNOWLEDGED',
    PLAYING = 'PLAYING',
    LOST = 'LOST',
    YIELDED = 'YIELDED',
    WON = 'WON',
}

export class Player {
    public readonly id: number;
    public readonly status: PlayerStatus;
    public readonly coordinates: Coordinates | undefined;

    protected constructor(id: number, status: PlayerStatus, coordinates: Coordinates | undefined) {
        this.id = id;
        this.status = status;
        this.coordinates = coordinates;
    }

    protected setStatus(status: PlayerStatus): Player {
        return new Player(
            this.id,
            status,
            this.coordinates,
        );
    }

    public setCoordinates = (coordinates: Coordinates | undefined) => new Player(
        this.id,
        this.status,
        coordinates,
    )

    public static create = (playerId: number, coordinates: Coordinates) =>
        new Player(
            playerId,
            PlayerStatus.CREATED,
            coordinates,
        )

    public acknowledge = () => this.setStatus(PlayerStatus.ACKNOWLEDGED);

    public makePlaying = () => this.setStatus(PlayerStatus.PLAYING);

    public makeYield = () => new Player(
        this.id,
        PlayerStatus.YIELDED,
        undefined,
    )

    public makeLose = () => this.setStatus(PlayerStatus.LOST);

    public makeWin = () => this.setStatus(PlayerStatus.WON);
}

export type PlayerMap = FMap<number, Player>;
export type PlayerArray = ReadonlyArray<Player>;

export type PlayerDelta = Readonly<{
    id: number;
    newStatus: PlayerStatus;
    newCoordinates: Coordinates | undefined;
    statusChanged: boolean;
    coordinatesChanged: boolean;
}>;