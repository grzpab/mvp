export class FMap<K, V> {

    protected _store: {
        [key: number]: V;
    };

    protected _mapValueToStoreKey: (v: V) => number;

    public constructor(
        _mapValueToStoreKey: (v: V) => number,
    ) {
        this._store = {};
        this._mapValueToStoreKey = _mapValueToStoreKey;
    }

    public put(value: V): FMap < K, V > {
        const storeKey = this._mapValueToStoreKey(value);

        this._store[storeKey] = value;
        
        return this;
    }

    public get(key: number): V | undefined {
        return this._store[key];
    }

    public size(): number {
        return Object.keys(this._store).length;
    }

    public putMap(otherMap: FMap<K, V>): FMap<K, V> {
        this._store = Object.assign(
            {},
            this._store,
            otherMap._store,
        );

        return this;
    }

    public putArray(array: ReadonlyArray<V>): FMap<K, V> {
        const store: {
            [key: number]: V;
        } = [];

        array.forEach(value => {
            store[this._mapValueToStoreKey(value)] = value;
        });

        this._store = this._store = Object.assign(
            {},
            this._store,
            store,
        );

        return this;
    }

    public values(): ReadonlyArray<V> {
        return Object.values(this._store);
    }
}