export type Coordinates = [number, number];

export const enum Direction {
    NORTH,
    SOUTH,
    WEST,
    EAST,
}

export function calculateNewCoordinates (
    oldCoordinates: Coordinates,
    direction: Direction
): Coordinates {
    const [x, y] = oldCoordinates;

    switch (direction) {
        case Direction.NORTH:
            return [x, y - 1];
        case Direction.SOUTH:
            return [x, y + 1];
        case Direction.WEST:
            return [x - 1, y];
        case Direction.EAST:
            return [x + 1, y];
        default:
            return oldCoordinates;
    }
}