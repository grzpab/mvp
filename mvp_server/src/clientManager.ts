import { OutgoingCommands, FMap } from 'mvp_engine';

export type Client = Readonly<{
    id: number;
    gameId: number | undefined;
    outgoingCommands: OutgoingCommands;  
}>;

export class ClientManager {
    protected clientMap: FMap<number, Client>;

    constructor() {
        this.clientMap = new FMap<number, Client>((client) => client.id);
    }

    public addClient(client: Client): Client {
        this.clientMap.put(client);

        return client;
    }

    public getClient(clientId: number): Client | undefined {
        return this.clientMap.get(clientId);
    }

    public getClients(): ReadonlyArray<Client> {
        return this.clientMap.values();
    }

    public clearOutgoingCommands(clientId: number): Client | undefined {
        const oldClient = this.clientMap.get(clientId);

        if (oldClient === undefined) {
            throw new Error();
        }

        const newClient = {
            ...oldClient,
            outgoingCommands: [],
        };

        this.clientMap.put(newClient);

        return newClient;
    }

    public appendOutgoingCommands(gameId: number, outgoingCommands: OutgoingCommands): void {
        const clients: ReadonlyArray<Client> = this.clientMap
            .values()
            .filter(client => client.gameId === gameId)
            .map(client => ({
                ...client,
                outgoingCommands: client.outgoingCommands.concat(outgoingCommands),
            }));
            
        this.clientMap.putArray(clients);
    }
}