import * as Random from 'random-js';
import {
    Engine2,
    OutgoingCommands,
    CreateGameCommand,
    IncomingCommand,
} from 'mvp_engine';

export class GameManager {
    protected engineMap: Map<number, Engine2>;
    protected engineIdGenerator: Random;

    public constructor() {
        this.engineMap = new Map<number, Engine2>();
        this.engineIdGenerator = new Random(Random.engines.mt19937().autoSeed());
    }

    public createEngine(createGameCommand: CreateGameCommand): [number, OutgoingCommands] {
        const engineId = this._createEngineId();

        const engine = new Engine2();
        this.engineMap.set(engineId, engine);
        
        const outgoingCommands = engine.executeIncomingCommands([createGameCommand]);

        return [engineId, outgoingCommands];
    }

    public executeIncomingCommands(engineId: number, command: IncomingCommand): OutgoingCommands {
        const engine = this.engineMap.get(engineId);

        if (engine === undefined) {
            return [];
        }

        return engine.executeIncomingCommands([command]);
    }

    public getEngine(gameId: number): Engine2 | undefined {
        return this.engineMap.get(gameId);
    }

    private _createEngineId(): number {
        // TODO allow it to fail and decide about the constant
        while (true) {
            const engineId = this.engineIdGenerator.integer(0, 1000000); // TODO

            if (!this.engineMap.has(engineId)) {
                return engineId;
            }
        }
    }
}