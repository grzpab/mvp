import { createTcpIpServer } from './tcpIpServer';
import { createWebSocketServer } from './webSocketServer';
import { Application } from './application';

const application = new Application();

const tcpIpServer = createTcpIpServer(application);
const webSocketServer = createWebSocketServer(application);

process.on('beforeExit', () => {
    console.log('before exit');

    tcpIpServer.close();
    webSocketServer.close();
});
