import * as net from 'net';
import * as WebSocket from 'ws';

export type TcpIpMetaSocket = Readonly<{
    type: 'TCP_IP_SOCKET',
    internalSocket: net.Socket;
}>;

export type WebMetaSocket = Readonly<{
    type: 'WEB_SOCKET',
    internalSocket: WebSocket;
}>;

export type MetaSocket = TcpIpMetaSocket | WebMetaSocket;