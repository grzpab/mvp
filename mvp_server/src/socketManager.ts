
import { ServerMessages, createPacket } from 'mvp_engine';
import { MetaSocket } from './metaSockets';

export class MetaSocketManager {
    protected metaSocketMap: Map<number, MetaSocket> = new Map();

    public setMetaSocket(clientId: number, metaSocket: MetaSocket) {
        this.metaSocketMap.set(clientId, metaSocket);
    }

    public sendServerMessages = (serverMessages: ServerMessages) => {
        serverMessages
        .forEach(serverMessage => {
            const { clientId } = serverMessage;
        
            const metaSocket = this.metaSocketMap.get(clientId);

            if (metaSocket === undefined) {
                return;
            }

            if (metaSocket.type === 'TCP_IP_SOCKET') {
                const buffer = createPacket(serverMessage);
                metaSocket.internalSocket.write(buffer);
            }
    
            if (metaSocket.type === 'WEB_SOCKET') {
                const data = JSON.stringify(serverMessage);
                metaSocket.internalSocket.send(data);
            }
        });
    }
}