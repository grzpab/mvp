import * as net from 'net';

import {
    SERVER_HOST,
    SERVER_PORT,
    getMessage,
    ClientMessage,
} from 'mvp_engine';

import { Application } from './application';

export function createTcpIpServer(application: Application) {
    return net.createServer( (socket: net.Socket) => {
        const { remoteAddress, remotePort } = socket;

        socket.on('data', async (data: Buffer) => {
            const message = getMessage(data);

            if (!message) {
                return;
            }

            const clientMessage = message as ClientMessage;

            application.handleClientMessage(clientMessage, {
                type: 'TCP_IP_SOCKET',
                internalSocket: socket,
            });
        });

        socket.on('close', function () {
            console.log(`SOCKET CLOSED: ${remoteAddress}:${remotePort}`);
        });

    }).listen(SERVER_PORT, SERVER_HOST);
}