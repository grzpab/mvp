import * as WebSocket from 'ws';

import {
    WEB_SOCKET_SERVER_PORT,
    ClientMessage
} from 'mvp_engine';

import { Application } from './application';

export function createWebSocketServer(application: Application) {
    const server = new WebSocket.Server({ port: WEB_SOCKET_SERVER_PORT });

    server.on('connection', (socket) => {
        socket.on('message', async (stringifiedMessage) => {

            const clientMessage = JSON.parse(stringifiedMessage.toString()) as ClientMessage;

            application.handleClientMessage(clientMessage, {
                type: 'WEB_SOCKET',
                internalSocket: socket,
            });
        });

        socket.on('error', (err) => {
            console.error(err);
        });
    });

    server.on('error', (err) => {
        console.error(err.stack);
    });

    return server;
}
