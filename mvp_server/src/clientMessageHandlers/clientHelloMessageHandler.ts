import { Client } from '../clientManager';
import { ServerMessages, ClientHelloMessage, ServerHelloMessage } from 'mvp_engine';
import { Application } from '../application';
import { MetaSocket } from '../metaSockets';

export const clientHelloMessageHandler = (
    application: Application,
    clientMessage: ClientHelloMessage,
    metaSocket: MetaSocket,
): ServerMessages => {
    const getServerMessage = (clientId: number): ServerHelloMessage => ({
        type: 'SERVER_HELLO',
        clientId,
    });

    const createNewClient = () => {
        const client: Client = {
            id: 100, // TODO get id
            gameId: undefined,
            outgoingCommands: [],
        };

        application.clients.addClient(client);
        application.metaSockets.setMetaSocket(client.id, metaSocket);

        return getServerMessage(client.id);
    };

    const { clientId: previousClientId } = clientMessage;

    if (previousClientId === undefined) {
        return [createNewClient()];
    }

    const previousClient = application.clients.getClient(previousClientId);

    if (previousClient === undefined ) {
        return [createNewClient()];
    }

    return [getServerMessage(previousClientId)];
};