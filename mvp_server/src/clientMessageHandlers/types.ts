import {  ServerMessages, ClientMessage, ClientMessageType } from 'mvp_engine';
import { Application } from '../application';
import { MetaSocket } from '../metaSockets';

export type ClientMessageHandler = (
    application: Application,
    clientMessage: ClientMessage,
    metaSocket: MetaSocket,
) => ServerMessages;

export type MessageHandlers = Map<ClientMessageType, ClientMessageHandler>;