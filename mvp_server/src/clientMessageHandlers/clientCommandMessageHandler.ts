import { CommandType, ClientCommandMessage, ServerMessages } from 'mvp_engine';
import { Application } from '../application';

export const clientCommandMessageHandler = (
    application: Application,
    clientMessage: ClientCommandMessage,
): ServerMessages => {
    const { clientId, commands } = clientMessage;

    const client = application.clients.getClient(clientId);
    
    if (client === undefined) {
        return [];
    }

    commands.forEach(command => {
        switch (command.commandType) {
            case CommandType.CREATE_GAME:
            {
                const [engineId, outgoingCommands] = application.games.createEngine(command);

                application.clients.appendOutgoingCommands(engineId, outgoingCommands);
                break;
            }
            case CommandType.JOIN_GAME:
            case CommandType.START_GAME:
            case CommandType.ACKNOWLEDGE_GAME:
            case CommandType.MOVE_PLAYER:
            case CommandType.YIELD_GAME:
            {
                const { gameId } = clientMessage;

                if (gameId === undefined) {
                    break;
                }
    
                const outgoingCommands = application.games.executeIncomingCommands(gameId, command);
    
                application.clients.appendOutgoingCommands(gameId, outgoingCommands);
                break;
            }
            default:
                break;
        }
    });
    
    return []; // we don't emit events here
};
