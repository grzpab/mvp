import { MessageHandlers, ClientMessageHandler } from './types';
import { clientHelloMessageHandler } from './clientHelloMessageHandler';
import { clientPingMessageHandler } from './clientPingMessageHandler';
import { ClientMessageType } from 'mvp_engine';
import { clientCommandMessageHandler } from './clientCommandMessageHandler';

const entries: ReadonlyArray<[ClientMessageType, ClientMessageHandler]> = [
    ['CLIENT_HELLO', clientHelloMessageHandler],
    ['CLIENT_PING', clientPingMessageHandler],
    ['CLIENT_COMMAND', clientCommandMessageHandler],
];

export const messageHandlers: MessageHandlers = new Map(entries);
