import { ClientPingMessage, ServerPongMessage, ServerMessages } from 'mvp_engine';
import { Application } from '../application';

export const clientPingMessageHandler = (
    application: Application,
    clientMessage: ClientPingMessage,
): ServerMessages => {
    const { clientId, gameId } = clientMessage;

    const client = application.clients.getClient(clientId);

    if (client === undefined || client.gameId !== gameId) {
        return [];
    }

    const { outgoingCommands } = client;

    const serverMessage: ServerPongMessage = {
        type: 'SERVER_PONG',
        clientId,
        gameId,
        commands: outgoingCommands,
    };
    
    application.clients.clearOutgoingCommands(clientId);

    return [serverMessage];
};