import { ClientManager } from './clientManager';
import { MetaSocketManager } from './socketManager';
import { GameManager } from './gameManager';
import { ClientMessage } from 'mvp_engine';
import { ClientMessageHandler } from './clientMessageHandlers/types';
import { messageHandlers } from './clientMessageHandlers';
import { MetaSocket } from './metaSockets';

export class Application {
    // TODO dependency injection
    public clients: ClientManager = new ClientManager();
    public games: GameManager = new GameManager();
    public metaSockets: MetaSocketManager = new MetaSocketManager();

    public handleClientMessage(clientMessage: ClientMessage, metaSocket: MetaSocket) {
        const handler: ClientMessageHandler | undefined = messageHandlers.get(clientMessage.type);

        if (handler === undefined) {
            return;
        }

        const serverMessages = handler(this, clientMessage, metaSocket);

        this.metaSockets.sendServerMessages(serverMessages);
    }
}