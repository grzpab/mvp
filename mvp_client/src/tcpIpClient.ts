import * as net from 'net';
import { ClientHelloMessage, createPacket, getMessage } from 'mvp_engine';

export function createTcpIpClient() {
    const options = {
        host: '127.0.0.1',
        port: 12121,
    };

    const client = net.createConnection(options, () => {
        console.log('connected');

        const clientHelloMessage: ClientHelloMessage = {
            type: 'CLIENT_HELLO',
            clientId: undefined,
        };

        const buffer = createPacket(clientHelloMessage);

        client.write(buffer);
    });

    client.on('data', (data) => {
        const message = getMessage(data);

        console.log(message);

        client.destroy();
    });
}